/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/4/4 11:16:21                            */
/*==============================================================*/


drop table if exists PO_APPLICATION;

drop table if exists PO_MODULE;

drop table if exists PO_ROLE;

drop table if exists PO_ROLE_RIGHT;

drop table if exists PO_ROLE_USER;

drop table if exists PO_USER_LOGIN;

/*==============================================================*/
/* Table: PO_APPLICATION                                        */
/*==============================================================*/
create table PO_APPLICATION
(
   APP_ID               bigint not null auto_increment,
   APP_NAME             varchar(30),
   APP_CODE         varchar(63) ,
    TYPE           varchar(11) DEFAULT NULL COMMENT 'TYPE：0-普通应用；1-系统应用',
   primary key (APP_ID)
);

/*==============================================================*/
/* Table: PO_MODULE                                             */
/*==============================================================*/
create table PO_MODULE
(
   MODULE_ID            bigint not null auto_increment,
   APP_ID               bigint,
   APP_NAME  varchar(63) ,
   MODULE_NAME          varchar(30),
   MODULE_CODE varchar(63) DEFAULT NULL,
   primary key (MODULE_ID)
);

/*==============================================================*/
/* Table: PO_ROLE                                               */
/*==============================================================*/
create table PO_ROLE
(
   ROLE_ID              bigint not null auto_increment,
   ROLE_NAME            varchar(30),
   ROLE_CODE varchar(63),
   primary key (ROLE_ID)
);

/*==============================================================*/
/* Table: PO_ROLE_RIGHT                                         */
/*==============================================================*/
create table PO_ROLE_RIGHT
(
   ROLE_RIGHT_ID        bigint not null auto_increment,
   ROLE_ID              bigint,
   MODULE_ID            bigint,
   primary key (ROLE_RIGHT_ID)
);

/*==============================================================*/
/* Table: PO_ROLE_USER                                          */
/*==============================================================*/
create table PO_ROLE_USER
(
   ROLE_USER_ID         bigint not null auto_increment,
   ROLE_ID              bigint,
   USER_ID              bigint,
   primary key (ROLE_USER_ID)
);

/*==============================================================*/
/* Table: PO_USER_LOGIN                                         */
/*==============================================================*/
create table PO_USER_LOGIN
(
   USER_ID              bigint not null auto_increment,
   USER_LOGIN_ID        varchar(30),
   USER_PASSWORD_ID     varchar(30),
   USER_NAME            varchar(30),
   USER_EMAIL            varchar(30),
    USER_PHONE         varchar(30),
   IS_SYSTEM char(1) DEFAULT NULL COMMENT 'IS_SYSTEM：0-系统管理员；1-普通用户',
   primary key (USER_ID)
);
 INSERT INTO PO_USER_LOGIN VALUES('1','admin','123','系统管理员','admin@gmail.com','1860000000','0');
 INSERT INTO PO_USER_LOGIN VALUES('2','zhangsan','123','张三','zhangsan@gmail.com','1860000001','1');
 INSERT INTO PO_USER_LOGIN VALUES('3','lisi','123','李四','lisi@gmail.com','1860000002','1');

 INSERT INTO PO_APPLICATION VALUES('1','人力资源管理应用','HR','0'),('2','企业资源管理应用','ERP','0');


 INSERT INTO PO_MODULE VALUES('1','1','人力资源管理应用','招聘管理','zhaopin');
 INSERT INTO PO_MODULE VALUES('2','1','人力资源管理应用','培训管理','peixun');


 INSERT INTO PO_ROLE VALUES('1','测试角色1','role1'),('2','测试角色2','role2');
 INSERT INTO PO_ROLE VALUES('3','测试角色3','role3'),('4','测试角色4','role4');
 INSERT INTO PO_ROLE VALUES('5','测试角色5','role5');
 