<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="configureRole.jsp"/>
</head>
<body style="background-color: #fbfbfb" onload="load()">
<h1 style="color: transparent;
            background-color : black;
            text-shadow : rgba(255,255,255,0.5) 0 5px 6px, rgba(255,255,255,0.2) 1px 3px 3px;
            -webkit-background-clip : text;
            font-size: 25px;">角色管理</h1>
<div style="border:1px #8D8D8D solid;margin-top: 20px;width: 900px; border-radius:4px; margin-left: 150px;">
    <button class="layui-btn" id="editrole">修改角色权限</button>
    <button class="layui-btn" id="delrole" onclick="modeldel()">删除角色</button>
    <hr/>
</div>
<script src="/layui/layui.js"></script>
<table class="layui-hide" id="test" lay-filter="test"></table>
<script>
    function load(){
        layui.use('table', function(){
            var table = layui.table;
            table.render({
                elem: '#test'
                ,url:'${pageContext.request.contextPath}/toRoleList'
                ,cols: [[
                    {type:'radio'}
                    ,{field:'roleId', width:320, title: '角色ID', sort: true}
                    ,{field:'roleName', width:360, title: '角色名称',sort: true}
                    ,{field:'roleCode', width:330, title: '角色代码',sort: true}
                ]]
            });
        });}
</script>
<script>
    function  modeldel(){
        var selectData = layui.table.checkStatus('test').data;
        var roleId=selectData[0].roleId;
        $.ajax({
            url: "${pageContext.request.contextPath }/roleDel",
            type: "post",
            //定义回调响应的数据格式为JSON字符串,该属性可以省略
            data:JSON.stringify(
                {
                    roleId:roleId,
                }
            ),
            contentType:"application/json;charset=UTF-8",
            dataType: "json",
            //成功响应的结果
        })
        alert("提示：删除角色成功")
        load();
    }
    //加载左边并修改右边的框
    function edit() {
        var selectData = layui.table.checkStatus('test').data;
        var roleid=selectData[0].roleId;
        document.frmeditrole.rolename.value=selectData[0].roleName;
        document.frmeditrole.rolecode.value=selectData[0].roleCode;
        // 获取输入的应用信息
        $.ajax({
            url: "${pageContext.request.contextPath }/roleright",
            type: "post",
            data:JSON.stringify(
                {
                    roleId:roleid,
                }
            ),
            contentType:"application/json;charset=UTF-8",
            dataType: "json",
            //成功响应的结果
            success: function (date) {
                for(var j=0;j<date.data.length;j++){
                    $('#leftbox').append("<option value="+date.data[j].moduleId+">"+date.data[j].moduleName+"</option>");
                }
                //判断右边和左边不能重复
                for(var i=0;i<$('#leftbox option').length;i++) {
                    for(var j=0;j<$('#rightbox option').length;j++){
                        if($('#leftbox option').get(i).text==$('#rightbox option').get(j).text){
                            $("#rightbox option").get(j).remove();
                        }
                    }
                }
            }
        })
    }
</script>
<script>
    //打开修改角色权限模态框
    (function() {
        /*建立模态框对象*/
        var modalBox = {};
        /*获取模态框*/
        modalBox.modal = document.getElementById("myModal");
        /*获得trigger按钮*/
        modalBox.triggerBtn = document.getElementById("editrole");
        /*获得关闭按钮*/
        modalBox.closeBtn = document.getElementById("closeBtn");
        /*模态框显示*/
        modalBox.show = function() {
            console.log(this.modal);
            this.modal.style.display = "block";
            var selectData = layui.table.checkStatus('test').data;
            load2();
            edit();
            //实现重置修改角色界面
            $('#reset2').click(function () {
                load2();
                edit();
            })
            //实现修改信息
            $("#btneditbyjson").click(function() {
                // 获取输入的图书信息
                var roleId=selectData[0].roleId;
                var roleName =$("#rolename").val();
                var roleCode = $("#rolecode").val();
                var moduleids = $("#leftbox option").map(function(){
                    return $(this).val();
                }).get().join(",");
                $.ajax({
                    url: "${pageContext.request.contextPath }/roleEdit",
                    type: "post",
                    // data表示发送的数据
                    // data:"&userLoginId="+userLoginId+"&userPasswordId="+userPasswordId+"&userName="+userName+"&userEmail="+userEmail+"&userPhone="+userPhone+"&isSystem="+isSystem,
                    //定义回调响应的数据格式为JSON字符串,该属性可以省略
                    data:JSON.stringify(
                        {
                            roleId:roleId,
                            roleName:roleName,
                            roleCode:roleCode,
                            moduleids:moduleids
                        }
                    ),
                    contentType:"application/json;charset=UTF-8",
                    dataType: "json",
                })
                modalBox.close();
            })
        }
        /*模态框关闭*/
        modalBox.close = function() {
            $('#cancel1').trigger("click");
            load();
        }
        /*模态框初始化*/
        modalBox.init = function() {
            var that = this;
            this.triggerBtn.onclick = function() {
                that.show();
            }
            this.closeBtn.onclick = function() {
                that.close();
            }
        }
        modalBox.init();
    })();

</script>
</body>
</html>
