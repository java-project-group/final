<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="userRole.jsp"/>
</head>
<body style="background-color: #fbfbfb" onload="load()">
<h1 style="color: transparent;
            background-color : black;
            text-shadow : rgba(255,255,255,0.5) 0 5px 6px, rgba(255,255,255,0.2) 1px 3px 3px;
            -webkit-background-clip : text;
            font-size: 25px;">授权管理</h1>
<div style="border:1px #8D8D8D solid;margin-top: 50px;width: 900px; border-radius:4px; margin-left: 150px; text-align: center">
    <button class="layui-btn" id="userRole">授予人员角色</button>
    <hr/>
</div>
<script src="/layui/layui.js"></script>
<table class="layui-hide" id="test" lay-filter="test"></table>
<script>
    function load(){
        layui.use('table', function(){
            var table = layui.table;
            table.render({
                elem: '#test'
                ,url:'${pageContext.request.contextPath}/toRoleUserList'
                ,cols: [[
                    {type:'radio'}
                    ,{field:'userLoginId', width:200, title: '用户名',sort: true}
                    ,{field:'userName', width:220, title: '姓名',sort: true}
                    ,{field:'rolenames', width:350, title: '已分配角色',sort: true}
                    ,{field:'isSystem', width:200, title: '账户类型', sort: true}
                ]]
            });
        });}
</script>
<script>
    function edit() {
        var selectData = layui.table.checkStatus('test').data;
        var userid=selectData[0].userId;
        $('#rightbox').empty();
        $('#leftbox').empty();
        // 获取输入的应用信息
        $.ajax({
            url: "${pageContext.request.contextPath }/roleuser",
            type: "post",
            data:JSON.stringify(
                {
                    userId:userid,
                }
            ),
            contentType:"application/json;charset=UTF-8",
            dataType: "json",
            //成功响应的结果
            success: function (date) {
                for(var j=0;j<date.data.length;j++){
                    $('#leftbox').append("<option value="+date.data[j].roleId+">"+date.data[j].roleName+"</option>");
                }
                //判断右边和左边不能重复
                for(var i=0;i<$('#leftbox option').length;i++) {
                    for(var j=0;j<$('#rightbox option').length;j++){
                        if($('#leftbox option').get(i).text==$('#rightbox option').get(j).text){
                            $("#rightbox option").get(j).remove();
                        }
                    }
                }
            }
        })
    }
</script>
<script>
    //打开给人员添加角色模态框
    (function() {
        /*建立模态框对象*/
        var modalBox = {};
        /*获取模态框*/
        modalBox.modal = document.getElementById("myModal");
        /*获得trigger按钮*/
        modalBox.triggerBtn = document.getElementById("userRole");
        /*获得关闭按钮*/
        modalBox.closeBtn = document.getElementById("closeBtn");
        /*模态框显示*/
        modalBox.show = function() {
            console.log(this.modal);
            this.modal.style.display = "block";
            var selectData = layui.table.checkStatus('test').data;
            load2();
            edit();
            $('#reset2').click(function () {
                load2();
                edit();
            })
            //实现修改信息
            $("#btneditbyjson").click(function() {
                // 获取输入的人员角色信息
                var userid=selectData[0].userId;
                var roleids = $("#leftbox option").map(function(){
                    return $(this).val();
                }).get().join(",");
                $.ajax({
                    url: "${pageContext.request.contextPath }/roleuserEdit",
                    type: "post",
                    //定义回调响应的数据格式为JSON字符串,该属性可以省略
                    data:JSON.stringify(
                        {
                            userId:userid,
                            roleids:roleids
                        }
                    ),
                    contentType:"application/json;charset=UTF-8",
                    dataType: "json",
                })
                alert("授权成功！");
                modalBox.close();
            })
        }
        /*模态框关闭*/
        modalBox.close = function() {
            $('#cancel1').trigger("click");
            load();
        }
        /*当用户点击模态框内容之外的区域，模态框也会关闭*/
        modalBox.outsideClick = function() {
            var modal = this.modal;
            window.onclick = function(event) {
                if(event.target == modal) {
                    modal.style.display = "none";
                }
            }
        }
        /*模态框初始化*/
        modalBox.init = function() {
            var that = this;
            this.triggerBtn.onclick = function() {
                that.show();
            }
            this.closeBtn.onclick = function() {
                that.close();
            }
            this.outsideClick();
        }
        modalBox.init();
    })();

</script>
</body>
</html>
