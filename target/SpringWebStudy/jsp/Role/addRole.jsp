<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../layui/css/layui.css">
</head>
<body onload="load2()">
<h1 style="color: transparent;
            background-color : black;
            text-shadow : rgba(255,255,255,0.5) 0 5px 6px, rgba(255,255,255,0.2) 1px 3px 3px;
            -webkit-background-clip : text;
            font-size: 25px;">添加角色</h1>

    <div style="background: seashell">
        <div style="margin-left: 150px">
            <div class="layui-inline">
                <label class="layui-form-label" style="margin-top: 30px;margin-left: 100px">角色名称</label>
                <div class="layui-input-inline" style="margin-top: 30px">
                    <input type="tel" id="rolename" name="rolename" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline" >
                <label class="layui-form-label" style="margin-top: 30px">角色代码</label>
                <div class="layui-input-inline" style="margin-top: 30px">
                    <input type="tel" id="rolecode" name="rolecode" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
            <br><br><br><br>
            <div style="margin-left: 200px">
                <div  style="float:left">
                    <optgroup label="已分配权限"></optgroup>
                    <select id="leftbox" name="leftbox"style="width: 400px;height: 250px;font-size: 20px;" multiple="multiple">
                    </select>
                </div>
                <div  style="float: left">
                    <button class="layui-btn" style="margin-top: 50px" onclick="move('rightbox','leftbox')">←添加</button>
                    <br><button class="layui-btn" style="margin-top: 70px" onclick="move('leftbox','rightbox')">→移除</button>
                </div>
                <div  style="float: left">
                    <optgroup label="可分配权限"></optgroup>
                    <select id="rightbox" name="rightbox" style="width: 400px;height: 250px;font-size: 20px;" multiple="multiple">

                    </select>
                </div>
            </div>
        </div>
        <br><br>
        <div class="layui-form-item" style="margin-left: 400px">
            <div class="layui-input-block">
            <button class="layui-btn" onclick="add()">添加</button>
            <input type="button" class="layui-btn" id="reset2" name="resert2" value="重置"/>
            <button class="layui-btn" id="cancel">取消</button>
            </div>
    </div>

</body>

 <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js">
</script>
<script>
    // 移动id为from的列表中的选中项到id为to的列表中
    function move(from,to) {
        // 获取移动源
        var fromBox = document.getElementById(from);
        // 获取移动目的地
        var toBox = document.getElementById(to);
        // 当移动源存在选中项时
        while(fromBox.selectedIndex != -1){
            // 将移动源中的选中项添加到移动目的地的末尾
            toBox.appendChild(fromBox.options[fromBox.selectedIndex]);
        }
    }
    // 移动id为from的列表中的所有项到id为to的列表中
    function moveAll(from,to) {
        // 获取移动源
        var fromBox = document.getElementById(from);
        // 获取移动目的地
        var toBox = document.getElementById(to);
        // 当移动源存在选中项时
        while(fromBox.options.length > 0){
            // 将移动源中的选中项添加到移动目的地的末尾
            toBox.appendChild(fromBox.options[0]);
        }
    }
    //实现重置添加角色界面
    $('#reset2').click(function () {
        load2();
    })
    //添加按钮
    function add() {
        var roleName =$("#rolename").val();
        var roleCode = $("#rolecode").val();
        var moduleids = $("#leftbox option").map(function(){
            return $(this).val();
        }).get().join(",");
        $.ajax({
            url: "${pageContext.request.contextPath }/roleAdd",
            type: "post",
            //定义回调响应的数据格式为JSON字符串,该属性可以省略
            data:JSON.stringify(
                {
                    roleName:roleName,
                    roleCode:roleCode,
                    moduleids:moduleids
                }
            ),
            contentType:"application/json;charset=UTF-8",
            dataType: "json",
            //成功响应的结果
        })
        alert("提示：添加角色成功")
    }
    ////加载右边可分配权限
    function load2(){
        $("#rightbox").find("option:selected").text("");
        $("#leftbox").find("option:selected").text("");
        $('#rightbox').empty();
        $('#leftbox').empty();
        // 获取输入的应用信息
        $.ajax({
            url: "${pageContext.request.contextPath}/toRoleAdd",    //后台controller里的方法名称
            contentType: "application/json; charset=utf-8",
            type: "get",
            async : true ,
            dataType: "json",
            success: function (date) {
                for (var j = 0; j <date.data.length;j++) {
                    $("#rightbox").append("<option value="+date.data[j].moduleId+">"+date.data[j].moduleName+"</option>");
            }
            }
        });
    }
</script>
</html>
