<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--引入模态框的页面--%>
    <jsp:include page="appadd.jsp"/>
    <jsp:include page="appedit.jsp"/>
    <jsp:include page="manage.jsp"/>
</head>
<body style="background-color: #fbfbfb" onload="load()" >
<div style="border:1px #8D8D8D solid;margin-top: 20px;width: 900px; border-radius:4px; margin-left: 150px;">
    <button class="layui-btn" id="addapp" style="margin-left: 200px">添加应用</button>
    <button class="layui-btn" id="editapp">修改信息</button>
    <button class="layui-btn" id="delapp" onclick="appdel()">删除应用</button>
    <button class="layui-btn" id="manage" onclick="manage()">管理模块</button>
    <hr/>
</div>
<script src="/layui/layui.js"></script>
<table class="layui-hide" id="test" lay-filter="test"></table>
<script>
    //初始化函数，出现列表
    function load(){
        layui.use('table', function(){
            var table = layui.table;
            table.render({
                elem: '#test'
                ,url:'${pageContext.request.contextPath}/toAppList'
                ,cols: [[
                    {type:'radio'}
                    ,{field:'appId', width:200, title: 'ID', sort: true}
                    ,{field:'appName', width:370, title: '应用名称',sort: true}
                    ,{field:'appCode', width:335, title: '应用代码', sort:true}
                    ,{field:'type', width:405, title: '应用类型',sort: true}
                ]]
            });
        });}
</script>
<script>
    //打开添加应用信息模态框
    (function() {
        /*建立模态框对象*/
        var modalBox = {};
        /*获取模态框*/
        modalBox.modal = document.getElementById("myModal");
        /*获得trigger按钮*/
        modalBox.triggerBtn = document.getElementById("addapp");
        /*获得关闭按钮*/
        modalBox.closeBtn = document.getElementById("closeBtn");
        /*模态框显示*/
        modalBox.show = function() {
            console.log(this.modal);
            //模态框显示
            this.modal.style.display = "block";
            //清空表单里面的数据，函数在appadd里面
            clearForm('#frmaddapp');
            //实现重置添加应用页面
            $('#reset1').click(function () {
                clearForm('#frmaddapp');
            })
            //实现添加应用
            $("#btnaddbyjson").click(function() {
                // 获取输入的应用信息
                var appName =$("#appName").val();
                var appCode = $("#appCode").val();
                var type1= $("#type").val();
                $.ajax({
                    url: "${pageContext.request.contextPath }/appAdd",
                    type: "post",
                    //定义回调响应的数据格式为JSON字符串,该属性可以省略
                    data:JSON.stringify(
                        {
                            appName:appName,
                            appCode:appCode,
                            type:type1
                        }
                    ),
                    contentType:"application/json;charset=UTF-8",
                    dataType: "json",
                    //成功响应的结果
                })
                alert("提示：添加应用成功")
                modalBox.close();
            })
        }
        /*模态框关闭*/
        modalBox.close = function() {
            this.modal.style.display = "none";
            load();
        }
        /*模态框初始化*/
        modalBox.init = function() {
            var that = this;
            this.triggerBtn.onclick = function() {
                that.show();
            }
            this.closeBtn.onclick = function() {
                that.close();
            }
        }
        modalBox.init();
    })();
    //打开修改信息模态框
    (function() {
        /*建立模态框对象*/
        var modalBox = {};
        /*获取模态框*/
        modalBox.modal = document.getElementById("myModal1");
        /*获得trigger按钮*/
        modalBox.triggerBtn = document.getElementById("editapp");
        /*获得关闭按钮*/
        modalBox.closeBtn = document.getElementById("closeBtn2");
        /*模态框显示*/
        modalBox.show = function() {
            console.log(this.modal);
            this.modal.style.display = "block";
            clearForm('#frmeditapp');
            //实现重置修改用户页面
            $('#reset2').click(function () {
                clearForm('#frmeditapp');
            })
            //实现数据回填
            var selectData = layui.table.checkStatus('test').data;
            document.frmeditapp.appName1.value=selectData[0].appName;
            document.frmeditapp.appCode1.value=selectData[0].appCode;
            document.frmeditapp.type1.value=selectData[0].type;
            if(selectData[0].type==1)
            {
                document.frmeditapp.type1.value="系统应用";
            }else if(selectData[0].type==0){
                document.frmeditapp.type1.value="普通应用";
            }
            //实现修改信息
            $("#btneditbyjson").click(function() {
                // 获取输入的应用信息
                var appId=selectData[0].appId;
                var appName =$("#appName1").val();
                var appCode = $("#appCode1").val();
                var type= $("#type1").val();
                if(type=="系统应用")
                {
                    type=1;
                }else if(type=="普通应用"){
                    type=0;
                }
                $.ajax({
                    url: "${pageContext.request.contextPath }/appEdit",
                    type: "post",
                    //定义回调响应的数据格式为JSON字符串,该属性可以省略
                    data:JSON.stringify(
                        {
                            appId:appId,
                            appName:appName,
                            appCode:appCode,
                            type:type
                        }
                    ),
                    contentType:"application/json;charset=UTF-8",
                    dataType: "json",
                })
                alert("提示：修改信息成功！")
                modalBox.close();
            })
        }
        /*模态框关闭*/
        modalBox.close = function() {
            this.modal.style.display = "none";
            load();
        }
        /*当用户点击模态框内容之外的区域，模态框也会关闭*/
        modalBox.outsideClick = function() {
            var modal = this.modal;
            window.onclick = function(event) {
                if(event.target == modal) {
                    modal.style.display = "none";
                }
            }
        }
        /*模态框初始化*/
        modalBox.init = function() {
            var that = this;
            this.triggerBtn.onclick = function() {
                that.show();
            }
            this.closeBtn.onclick = function() {
                that.close();
            }
            this.outsideClick();
        }
        modalBox.init();
    })();


    //模块管理
    (function() {
        /*建立模态框对象*/
        var modalBox = {};
        /*获取模态框*/
        modalBox.modal = document.getElementById("myModal2");
        /*获得trigger按钮*/
        modalBox.triggerBtn = document.getElementById("manage");
        /*获得关闭按钮*/
        modalBox.closeBtn = document.getElementById("closeBtn3");
        /*模态框显示*/
        modalBox.show = function() {
            console.log(this.modal);
            this.modal.style.display = "block";
            $('#rightbox').empty();
            var selectData = layui.table.checkStatus('test').data;
            var appid=selectData[0].appId;
                // 获取输入的应用信息
                $.ajax({
                    url: "${pageContext.request.contextPath }/managemodule",
                    type: "post",
                    data:JSON.stringify(
                        {
                            appId:appid,
                        }
                    ),
                    contentType:"application/json;charset=UTF-8",
                    dataType: "json",
                    //成功响应的结果
                    success: function (date) {
                        for(var j=0;j<date.data.length;j++){
                            $('#rightbox').append($("<option>").text(date.data[j].moduleName));
                        }
                    }
                })
        }
        /*模态框关闭*/
        modalBox.close = function() {
            this.modal.style.display = "none";
        }
        /*模态框初始化*/
        modalBox.init = function() {
            var that = this;
            this.triggerBtn.onclick = function() {
                that.show();
            }
            this.closeBtn.onclick = function() {
                that.close();
            }
        }
        modalBox.init();
    })();

    function appdel() {
        var selectData = layui.table.checkStatus('test').data;
        var appId=selectData[0].appId;
        $.ajax({
            url: "${pageContext.request.contextPath }/appDel",
            type: "post",
            //定义回调响应的数据格式为JSON字符串,该属性可以省略
            data:JSON.stringify(
                {
                    appId:appId
                }
            ),
            contentType:"application/json;charset=UTF-8",
            dataType: "json",
        })
        alert("提示：删除成功！");
        load();
    }
</script>
</body>
</html>
