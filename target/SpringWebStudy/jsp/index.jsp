<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>layout 后台大布局 - Layui</title>
    <link rel="stylesheet" href="../layui/css/layui.css">
    <script src="../js/jquery-3.3.1.min.js" type="text/javascript" charset="UTF-8"></script>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">
            <img src="../img/hbxy.jpg" class="layui-nav-img">
            后台管理系统
        </div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="../img/hbxy.jpg" class="layui-nav-img">
                    ${USER_SESSION.userLoginId}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href=""></a></li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/loginText">登出</a></li>

        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul id="main-tree"class="layui-nav layui-nav-tree"  lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">
                        <img src="../img/1_1.jpg" class="layui-nav-img">组织人员管理
                    </a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;">组织管理</a></dd>
                        <dd><a href="javascript:adduser();">用户管理</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">
                        <img src="../img/3_1.jpg" class="layui-nav-img">应用管理
                    </a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:applist();">应用管理</a></dd>
                        <dd><a href="javascript:modulelist();">模块管理</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">
                        <img src="../img/4_1.jpg" class="layui-nav-img">权限管理
                    </a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:addrole();">添加角色</a></dd>
                        <dd><a href="javascript:role();">角色管理</a></dd>
                        <dd><a href="javascript:empower();">授权管理</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">
                        <img src="../img/2_1.jpg" class="layui-nav-img">个人中心
                    </a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;grmessage()">个人信息</a></dd>
                        <dd><a href="javascript:;editmessage()">修改密码</a></dd>
                    </dl>
                </li>
            </ul>
        </div>
    </div>

    <div id="main-body" class="layui-body">
        <!-- 内容主体区域 -->

    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
         @版权所有海滨学院软件1602-3小组
    </div>
</div>
<script src="../layui/layui.js"></script>
<script>
    //JavaScript代码区域
    layui.use('element', function() {
        var element = layui.element;
    });
    //显示中间区域，鉴权，显示不显示页面
    function adduser() {
        <%--if(0==${USER_SESSION.isSystem}){--%>
            document.getElementById("main-body").innerHTML = '<object type="text/html" ' +
                'data="User/User.jsp" width="100%" height="100%"></object>';
       // }else{
            <%--if("${USER_MODULE}".indexOf("用户管理")){--%>
                <%--document.getElementById("main-body").innerHTML = '<object type="text/html" ' +--%>
                    <%--'data="User/User.jsp" width="100%" height="100%"></object>';--%>
            <%--}else {--%>
            //     alert("当前用户没有权限！");
            <%--}--%>
        // }
    }
    function applist() {
        if(0==${USER_SESSION.isSystem}){
            document.getElementById("main-body").innerHTML = '<object type="text/html" ' +
                'data="app/appList.jsp" width="100%" height="100%"></object>'
        }else{
            <%--if("${USER_MODULE}".indexOf("应用管理")){--%>
                <%--document.getElementById("main-body").innerHTML = '<object type="text/html" ' +--%>
                    <%--'data="app/appList.jsp" width="100%" height="100%"></object>'--%>
        <%--}else {--%>
            alert("当前用户没有权限！");
        <%--}--%>
        }
    }
    function modulelist() {
        if(0==${USER_SESSION.isSystem}){
            document.getElementById("main-body").innerHTML = '<object type="text/html" ' +
                'data="module/modulelist.jsp" width="100%" height="100%"></object>'
        }else {
            <%--if("${USER_MODULE}".indexOf("模块管理")){--%>
                <%--document.getElementById("main-body").innerHTML = '<object type="text/html" ' +--%>
                    <%--'data="module/modulelist.jsp" width="100%" height="100%"></object>'--%>
            <%--}else {--%>
                alert("当前用户没有权限！");
            <%--}--%>
        }
    }
    function addrole() {
        if(0==${USER_SESSION.isSystem}){
            document.getElementById("main-body").innerHTML = '<object type="text/html" ' +
                'data="Role/addRole.jsp" width="100%" height="100%"></object>'
        }else {
            <%--if("${USER_MODULE}".indexOf("添加角色")){--%>
            <%--document.getElementById("main-body").innerHTML = '<object type="text/html" ' +--%>
            <%--'data="Role/addRole.jsp" width="100%" height="100%"></object>'--%>
            <%--}else {--%>
            alert("当前用户没有权限！");
            <%--}--%>
        }
    }
    function role() {
        if(0==${USER_SESSION.isSystem}){
            document.getElementById("main-body").innerHTML = '<object type="text/html" ' +
                'data="Role/Role.jsp" width="100%" height="100%"></object>'
        }else {
            <%--if("${USER_MODULE}".indexOf("角色管理")){--%>
                <%--document.getElementById("main-body").innerHTML = '<object type="text/html" ' +--%>
                    <%--'data="Role/Role.jsp" width="100%" height="100%"></object>'--%>
            <%--}else {--%>
                alert("当前用户没有权限！");
            <%--}--%>
        }
    }
    function empower() {
        if(0==${USER_SESSION.isSystem}){
            document.getElementById("main-body").innerHTML = '<object type="text/html" ' +
                'data="Role/empowerRole.jsp" width="100%" height="100%"></object>'
        }else {
            <%--if("${USER_MODULE}".indexOf("授权管理")){--%>
                <%--document.getElementById("main-body").innerHTML = '<object type="text/html" ' +--%>
                    <%--'data="Role/empowerRole.jsp" width="100%" height="100%"></object>'--%>
            <%--}else {--%>
                alert("当前用户没有权限！");
            <%--}--%>
        }
    }
    function grmessage() {
        document.getElementById("main-body").innerHTML = '<object type="text/html" ' +
            'data="message/grmessage.jsp" width="100%" height="100%"></object>'
    }
    function editmessage() {
        document.getElementById("main-body").innerHTML = '<object type="text/html" ' +
            'data="message/editmessage.jsp" width="100%" height="100%"></object>'
    }
</script>

</body>
</html>



