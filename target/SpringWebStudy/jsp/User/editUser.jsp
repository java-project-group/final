<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../layui/css/layui.css">
    <link rel="stylesheet" type="text/css" href="../../layui/css/modalBox.css">
</head>
<body>
<!-- 模态框 -->
<div id="myModal1" class="modal1">
    <div class="modal-content1">
        <div class="modal-header1">
            <h2>修改信息</h2>
            <span id="closeBtn2" class="close1">×</span>
        </div>
        <div class="modal-body1">
            <form name="frmedituser" id="frmedituser" method="post">
                <div style="padding: 15px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">用户名</label>
                            <div class="layui-input-inline">
                                <input type="text" name="userLoginId1" id="userLoginId1" lay-verify=""  autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">用户密码</label>
                            <div class="layui-input-inline">
                                <input type="password" name="userPasswordId1" id="userPasswordId1" lay-verify="pass" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">姓名</label>
                            <div class="layui-input-inline">
                                <input type="text" name="userName1" id="userName1" lay-verify=""  autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">邮箱</label>
                            <div class="layui-input-inline">
                                <input type="text" name="userEmail1"  id="userEmail1" lay-verify=""  autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">联系方式</label>
                            <div class="layui-input-inline">
                                <input type="text" name="userPhone1"  id="userPhone1" lay-verify=""  autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">账号类型</label>
                            <div class="layui-input-inline">
                                <input type="text" name="isSystem1" id="isSystem1" lay-verify=""  autocomplete="off" class="layui-input" placeholder="输入普通用户or系统管理员">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" id="btneditbyjson">修改</button>
                        <input type="button" name="reset2" id="reset2" class="layui-btn" value="重置 " lay-filter="demo1" >
                        <button class="layui-btn" lay-submit="" lay-filter="demo1" id="cancel2">取消</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script language="JavaScript">

</script>
</body>
