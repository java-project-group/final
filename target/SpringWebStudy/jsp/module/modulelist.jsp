<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--引入模态框的页面--%>
    <jsp:include page="moduleadd.jsp"/>
    <jsp:include page="moduleedit.jsp"/>
</head>
<body style="background-color: #fbfbfb" onload="load()" >
<div style="border:1px #8D8D8D solid;margin-top: 20px;width: 900px; border-radius:4px; margin-left: 150px;">
    <button class="layui-btn" id="addmodule" style="margin-left: 200px">添加模块</button>
    <button class="layui-btn" id="editmodule">修改模块</button>
    <button class="layui-btn" id="delmodule" onclick="modeldel()">删除模块</button>
    <hr/>
</div>
<script src="/layui/layui.js"></script>
<table class="layui-hide" id="test" lay-filter="test"></table>
<script>
    //初始化函数，出现列表
    function load(){
                layui.use('table', function(){
                    var table = layui.table;
                    table.render({
                        elem: '#test'
                        ,url:'${pageContext.request.contextPath}/toModuleList'
                        ,cols: [[
                            {type:'radio'}
                            ,{field:'moduleId', width:260, title: '模块ID', sort: true}
                            ,{field:'moduleName', width:345, title: '模块名称',sort: true}
                            ,{field:'moduleCode', width:295, title: '模块代码',sort: true}
                            ,{field:'appName', width:410, title: '所属应用',sort: true}
                        ]]
                    });
        });}
</script>
<script>

    //打开添加模块信息模态框
    (function() {
        /*建立模态框对象*/
        var modalBox = {};
        /*获取模态框*/
        modalBox.modal = document.getElementById("myModal");
        /*获得trigger按钮*/
        modalBox.triggerBtn = document.getElementById("addmodule");
        /*获得关闭按钮*/
        modalBox.closeBtn = document.getElementById("closeBtn");
        /*模态框显示*/
        modalBox.show = function() {
            console.log(this.modal);
            //显示
            this.modal.style.display = "block";
            //清空表单里面的数据，函数在appadd里面
            clearForm('#frmaddmodule');
            //实现重置添加模块页面
            $('#reset1').click(function () {
                clearForm('#frmaddmodule');
            })
            //实现添加模块
            $("#btnaddbyjson").click(function() {
                // 获取输入的模块信息
                var moduleName = $("#moduleName").val();
                var moduleCode= $("#moduleCode").val();
                var appName=$('#app option:selected').text();//选中的文本
                $.ajax({
                    url: "${pageContext.request.contextPath }/moduleAdd",
                    type: "post",
                    //定义回调响应的数据格式为JSON字符串,该属性可以省略
                    data:JSON.stringify(
                        {
                            moduleName:moduleName,
                            moduleCode:moduleCode,
                            appName: appName
                        }
                    ),
                    contentType:"application/json;charset=UTF-8",
                    dataType: "json",
                })
                alert("提示：添加模块成功！")
                modalBox.close();
            })
        }
        /*模态框关闭*/
        modalBox.close = function() {
            this.modal.style.display = "none";
            load();
        }
        /*当用户点击模态框内容之外的区域，模态框也会关闭*/
        modalBox.outsideClick = function() {
            var modal = this.modal;
            window.onclick = function(event) {
                if(event.target == modal) {
                    modal.style.display = "none";
                }
            }
        }
        /*模态框初始化*/
        modalBox.init = function() {
            load1();
            var that = this;
            this.triggerBtn.onclick = function() {
                that.show();
            }
            this.closeBtn.onclick = function() {
                that.close();
            }
            this.outsideClick();
        }
        modalBox.init();
    })();

    //打开修改信息模态框
    (function() {
        /*建立模态框对象*/
        var modalBox = {};
        /*获取模态框*/
        modalBox.modal = document.getElementById("myModal1");
        /*获得trigger按钮*/
        modalBox.triggerBtn = document.getElementById("editmodule");
        /*获得关闭按钮*/
        modalBox.closeBtn = document.getElementById("closeBtn2");
        /*模态框显示*/
        modalBox.show = function() {
            console.log(this.modal);
            //显示
            this.modal.style.display = "block";
            clearForm('#frmeditmodule');
            //实现重置修改用户页面
            $('#reset2').click(function () {
                clearForm('#frmeditmodule');
            })
            //实现数据回填
            var selectData = layui.table.checkStatus('test').data;
            document.frmeditmodule.moduleName1.value=selectData[0].moduleName;
            document.frmeditmodule.moduleCode1.value=selectData[0].moduleCode;
            document.frmeditmodule.appName1.value=selectData[0].appName;
            //实现修改信息
            $("#btneditbyjson").click(function() {
                // 获取输入的图书信息
                var moduleId=selectData[0].moduleId;
                var moduleName =$("#moduleName1").val();
                var moduleCode = $("#moduleCode1").val();
                var appName= $("#appName1").val();
                $.ajax({
                    url: "${pageContext.request.contextPath }/moduleEdit",
                    type: "post",
                    //定义回调响应的数据格式为JSON字符串,该属性可以省略
                    data:JSON.stringify(
                        {
                            moduleId:moduleId,
                            moduleName:moduleName,
                            moduleCode:moduleCode,
                            appName:appName
                        }
                    ),
                    contentType:"application/json;charset=UTF-8",
                    dataType: "json",
                })
                modalBox.close();
            })
        }
        /*模态框关闭*/
        modalBox.close = function() {
            this.modal.style.display = "none";
            load();
        }
        /*当用户点击模态框内容之外的区域，模态框也会关闭*/
        modalBox.outsideClick = function() {
            var modal = this.modal;
            window.onclick = function(event) {
                if(event.target == modal) {
                    modal.style.display = "none";
                }
            }
        }
        /*模态框初始化*/
        modalBox.init = function() {
            var that = this;
            this.triggerBtn.onclick = function() {
                that.show();
            }
            this.closeBtn.onclick = function() {
                that.close();
            }
            this.outsideClick();
        }
        modalBox.init();
    })();

//删除模块
    function modeldel() {
        var selectData = layui.table.checkStatus('test').data;
        var moduleId=selectData[0].moduleId;
        $.ajax({
            url: "${pageContext.request.contextPath }/modulerDel",
            type: "post",
            //定义回调响应的数据格式为JSON字符串,该属性可以省略
            data:JSON.stringify(
                {
                    moduleId:moduleId
                }
            ),
            contentType:"application/json;charset=UTF-8",
            dataType: "json",
        })
        alert("删除成功");
        load();
    }
</script>
</body>
</html>
