<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../layui/css/layui.css">
    <link rel="stylesheet" type="text/css" href="../../layui/css/modalBox1.css">
</head>
<body>
<div id="myModal" class="modal1">
    <div class="modal-content1">
        <div class="modal-header1">
            <h2>配置角色权限</h2>
            <span id="closeBtn" class="close1">×</span>
        </div>
        <form name="frmuserRole" id="frmuserRole" method="post">
            <div>
                <div  style="float:left">
                    <optgroup label="已分配权限"></optgroup>
                    <select id="leftbox" style="width: 400px;height: 250px;font-size: 20px;" multiple="multiple">
                    </select>
                </div>
                <div  style="float: left">
                    <input   type="button"  class="layui-btn" style="margin-top: 50px" onclick="move('rightbox','leftbox')" value="←添加"/>
                    <br><input  type="button"  class="layui-btn" style="margin-top: 70px" onclick="move('leftbox','rightbox')" value="→移除"/>
                </div>
                <div  style="float: left">
                    <optgroup label="可分配权限"></optgroup>
                    <select id="rightbox" style="width: 400px;height: 250px;font-size: 20px;" multiple="multiple">
                    </select>
                </div>
            </div>
            <div class="layui-form-item" style="margin-left: 220px">
                <div class="layui-input-block">
                    <button class="layui-btn" id="btneditbyjson" name="btneditbyjson">添加</button>
                    <input type="button" class="layui-btn" id="reset2" name="resert2" value="重置"/>
                    <button class="layui-btn" id="cancel1" name="cancel1">取消</button>
                </div>
            </div>
        </form>
    </div>
</div>

</body>
 <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js">
</script>
 <script type="text/javascript">
    // 移动id为from的列表中的选中项到id为to的列表中
    function move(from,to) {
        // 获取移动源
        var fromBox = document.getElementById(from);
        // 获取移动目的地
        var toBox = document.getElementById(to);
        // 当移动源存在选中项时
        while(fromBox.selectedIndex != -1){
            // 将移动源中的选中项添加到移动目的地的末尾
            toBox.appendChild(fromBox.options[fromBox.selectedIndex]);
        }
    }
    //加载已分配权限
    function load2(){
        $("#rightbox").find("option:selected").text("");
        $("#leftbox").find("option:selected").text("");
        $('#rightbox').empty();
        $('#leftbox').empty();
        // 获取输入的应用信息
        $.ajax({
            url: "${pageContext.request.contextPath}/toRoleuser",    //后台controller里的方法名称
            contentType: "application/json; charset=utf-8",
            type: "get",
            async : true ,
            dataType: "json",
            success: function (date) {
                for (var j = 0; j <date.data.length;j++) {
                    $("#rightbox").append("<option value="+date.data[j].roleId+">"+date.data[j].roleName+"</option>");
                }
            }
        });
    }

</script>
</html>
