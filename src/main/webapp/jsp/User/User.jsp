<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--引入模态框的页面--%>
    <jsp:include page="addUser.jsp"/>
    <jsp:include page="editUser.jsp"/>
</head>
<body style="background-color: #fbfbfb" onload="load()" >
   <div style="border:1px #8D8D8D solid;margin-top: 20px;width: 900px; border-radius:4px; margin-left: 150px;">
       <button class="layui-btn" id="adduser" style="margin-left: 200px">添加用户</button>
       <button class="layui-btn" id="edituser">修改信息</button>
       <button class="layui-btn" id="deluser" onclick="userdel()">删除用户</button>
       <button class="layui-btn" id="resetuser" onclick="resetpwd()">重置密码</button>
       <hr/>
   </div>
   <script src="/layui/layui.js"></script>
   <table class="layui-hide" id="test" lay-filter="test"></table>
   <script>
       //初始化函数，出现列表
       function load(){
           layui.use('table', function(){
           var table = layui.table;
           table.render({
               elem: '#test'
               ,url:'${pageContext.request.contextPath}/toUserList'
               ,cols: [[
                   {type:'radio'}
                   ,{field:'userId', width:150, title: 'ID', sort: true}
                   ,{field:'userLoginId', width:170, title: '用户名',sort: true}
                   ,{field:'userPasswordId', width:170, title: '密码', hide:true}
                   ,{field:'userName', width:170, title: '姓名',sort: true}
                   ,{field:'userPhone', width:200, title: '联系方式',sort: true}
                   ,{field:'userEmail', width:200, title: '邮箱',sort: true}
                   ,{field:'isSystem', width:200, title: '账户类型', sort: true}
               ]]
           });
       });}
   </script>
<script>
    //鉴权判断功能能用是否
    if(0==${USER_SESSION.isSystem}){
        //打开添加用户信息模态框
        (function() {
            /*建立模态框对象*/
            var modalBox = {};
            /*获取模态框*/
            modalBox.modal = document.getElementById("myModal");
            /*获得trigger按钮*/
            modalBox.triggerBtn = document.getElementById("adduser");
            /*获得关闭按钮*/
            modalBox.closeBtn = document.getElementById("closeBtn");
            /*模态框显示*/
            modalBox.show = function() {
                console.log(this.modal);
                //模态框显示
                this.modal.style.display = "block";
                //清空表单里面的数据，函数在addUser里面
                clearForm('#frmadduser')
                $("#btnaddbyjson").click(function() {
                    // 获取输入的图书信息
                    var userLoginId =$("#userLoginId").val();
                    var userPasswordId = $("#userPasswordId").val();
                    var userName= $("#userName").val();
                    var userEmail= $("#userEmail").val();
                    var userPhone=$("#userPhone").val();
                    var isSystem= $("#isSystem").val();
                    $.ajax({
                        url: "${pageContext.request.contextPath }/userAdd",
                        type: "post",
                        //定义回调响应的数据格式为JSON字符串,该属性可以省略
                        data:JSON.stringify(
                            {
                                userLoginId:userLoginId,
                                userPasswordId:userPasswordId,
                                userName:userName,
                                userEmail:userEmail,
                                userPhone:userPhone,
                                isSystem:isSystem
                            }
                        ),
                        contentType:"application/json;charset=UTF-8",
                        dataType: "json",
                    })
                    alert("提示：添加用户成功")
                    modalBox.close();
                })
                //实现重置添加用户页面
                $('#reset1').click(function () {
                    clearForm('#frmadduser');
                })
            }
            /*模态框关闭*/
            modalBox.close = function() {
                this.modal.style.display = "none";
                //关闭后刷新页面作用
                load();
            }
            /*模态框初始化*/
            modalBox.init = function() {
                var that = this;
                this.triggerBtn.onclick = function() {
                    that.show();
                }
                this.closeBtn.onclick = function() {
                    that.close();
                }
            }
            modalBox.init();
        })();

        //打开修改信息模态框
        (function() {
            /*建立模态框对象*/
            var modalBox = {};
            /*获取模态框*/
            modalBox.modal = document.getElementById("myModal1");
            /*获得trigger按钮*/
            modalBox.triggerBtn = document.getElementById("edituser");
            /*获得关闭按钮*/
            modalBox.closeBtn = document.getElementById("closeBtn2");
            /*模态框显示*/
            modalBox.show = function() {
                console.log(this.modal);
                this.modal.style.display = "block";
                //实现重置修改信息界面
                $('#reset2').click(function () {
                    clearForm('#frmedituser');
                })
                //获取id是test的table用来取选中项里面的值
                var selectData = layui.table.checkStatus('test').data;
                //修改页面数据回填
                document.frmedituser.userLoginId1.value=selectData[0].userLoginId;
                document.frmedituser.userPasswordId1.value=selectData[0].userPasswordId;
                document.frmedituser.userName1.value=selectData[0].userName;
                document.frmedituser.userEmail1.value=selectData[0].userEmail;
                document.frmedituser.userPhone1.value=selectData[0].userPhone;
                document.frmedituser.isSystem1.value=selectData[0].isSystem;

                //实现修改信息
                $("#btneditbyjson").click(function() {
                    // 获取输入的图书信息
                    var userId=selectData[0].userId;
                    var userLoginId =$("#userLoginId1").val();
                    var userPasswordId = $("#userPasswordId1").val();
                    var userName= $("#userName1").val();
                    var userEmail= $("#userEmail1").val();
                    var userPhone=$("#userPhone1").val();
                    var isSystem= $("#isSystem1").val();
                    if(isSystem=="系统管理员")
                    {
                        isSystem=0;
                    }else if(isSystem=="普通用户"){
                        isSystem=1;
                    }
                    $.ajax({
                        url: "${pageContext.request.contextPath }/userEdit",
                        type: "post",
                        //定义回调响应的数据格式为JSON字符串,该属性可以省略
                        data:JSON.stringify(
                            {
                                userId:userId,
                                userLoginId:userLoginId,
                                userPasswordId:userPasswordId,
                                userName:userName,
                                userEmail:userEmail,
                                userPhone:userPhone,
                                isSystem:isSystem
                            }
                        ),
                        contentType:"application/json;charset=UTF-8",
                        dataType: "json",
                    })
                    alert("提示：修改用户成功")
                    modalBox.close();
                })
            }
            /*模态框关闭*/
            modalBox.close = function() {
                this.modal.style.display = "none";
                load();
            }
            /*模态框初始化*/
            modalBox.init = function() {
                var that = this;
                this.triggerBtn.onclick = function() {
                    that.show();
                }
                this.closeBtn.onclick = function() {
                    that.close();
                }
            }
            modalBox.init();
        })();

    //重置密码
    function resetpwd() {
            var selectData = layui.table.checkStatus('test').data;
            // 获得选中的人员里面的数据等着返回给后台
            var userId=selectData[0].userId;
            var userLoginId =selectData[0].userLoginId;
            var userPasswordId = 111111;
            var userName=selectData[0].userName;
            var userEmail= selectData[0].userEmail;
            var userPhone=selectData[0].userPhone;
            var isSystem= selectData[0].isSystem;
            if(isSystem=="系统管理员")
            {
                isSystem=0;
            }else if(isSystem=="普通用户"){
                isSystem=1;
            }
            $.ajax({
                url: "${pageContext.request.contextPath }/passwordRevise",
                type: "post",
                //定义回调响应的数据格式为JSON字符串,该属性可以省略
                data:JSON.stringify(
                    {
                        userId:userId,
                        userLoginId:userLoginId,
                        userPasswordId:userPasswordId,
                        userName:userName,
                        userEmail:userEmail,
                        userPhone:userPhone,
                        isSystem:isSystem
                    }
                ),
                contentType:"application/json;charset=UTF-8",
                dataType: "json",
                //成功响应的结果
            })
            load();
    }

    function userdel() {
           var selectData = layui.table.checkStatus('test').data;
           var userId=selectData[0].userId;
           $.ajax({
               url: "${pageContext.request.contextPath }/userDel",
               type: "post",
               //定义回调响应的数据格式为JSON字符串,该属性可以省略
               data:JSON.stringify(
                   {
                       userId:userId,
                   }
               ),
               contentType:"application/json;charset=UTF-8",
               dataType: "json",
               //成功响应的结果
           })
           alert("提示：删除用户成功")
           load();
    }
  }
</script>
</body>
</html>
