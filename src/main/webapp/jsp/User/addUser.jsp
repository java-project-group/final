<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../layui/css/layui.css">
    <link rel="stylesheet" type="text/css" href="../../layui/css/modalBox.css">
</head>
<body>
<!-- 模态框 -->
<div id="myModal" class="modal1">
    <div class="modal-content1">
        <div class="modal-header1">
            <h2>注册用户</h2>
            <span id="closeBtn" class="close1">×</span>
        </div>
        <div class="modal-body1">
            <form name="frmadduser" id="frmadduser" method="post">
                <div style="padding: 15px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">用户名</label>
                            <div class="layui-input-inline">
                                <input type="text" name="userLoginId" Id="userLoginId" lay-verify=""  autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">用户密码</label>
                            <div class="layui-input-inline">
                                <input type="password" name="userPasswordId" Id="userPasswordId"  lay-verify="pass" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">姓名</label>
                            <div class="layui-input-inline">
                                <input type="text" name="userName" lay-verify=""  autocomplete="off" id="userName" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">邮箱</label>
                            <div class="layui-input-inline">
                                <input type="text" name="userEmail" lay-verify="" id="userEmail" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">联系方式</label>
                            <div class="layui-input-inline">
                                <input type="text" name="userPhone" lay-verify="" id="userPhone" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">账号类型</label>
                            <div class="layui-input-inline">
                                <input type="text" name="isSystem" lay-verify=""  autocomplete="off" id="isSystem" class="layui-input" placeholder="0:系统管理员,1:普通用户">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button name="btnaddbyjson" id="btnaddbyjson" class="layui-btn" >添加</button>
                        <input type="button" name="reset1" id="reset1" class="layui-btn" value="重置">
                        <button class="layui-btn" name="cancel1" id="cancel1">取消</button>
                    </div>
                </div>
            </form>
    </div>
    </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js">
</script>
<script language="JavaScript">
    function clearForm(form) {
        // input清空
        $(':input', form).each(function () {
            var type = this.type;
            var tag = this.tagName.toLowerCase(); // normalize case
            if (type == 'text' || type == 'password' || type == 'email' || tag == 'textarea') {
                this.value = "";
            }
        })
    }






</script>
</body>


