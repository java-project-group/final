<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../layui/css/layui.css">
    <link rel="stylesheet" type="text/css" href="../../layui/css/modalBox.css">
</head>
<body>
<!-- 模态框 -->
<div id="myModal" class="modal1">
    <div class="modal-content1">
        <div class="modal-header1">
            <h2>添加模块</h2>
            <span id="closeBtn" class="close1">×</span>
        </div>
        <div class="modal-body1">
            <form name="frmaddmodule" id="frmaddmodule" method="post">
                <div style="padding: 15px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">所属应用</label>
                            <div class="layui-input-inline">
                                <select name="app" Id="app"  autocomplete="off" class="layui-input">
                                    <option>请选择</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">模块名称</label>
                            <div class="layui-input-inline">
                                <input type="text" name="moduleName" Id="moduleName" lay-verify=""  autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">模块代码</label>
                            <div class="layui-input-inline">
                                <input type="text" name="moduleCode" Id="moduleCode"  lay-verify="pass" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn"  name="btnaddbyjson" id="btnaddbyjson">添加</button>
                        <input type="button" name="reset1" id="reset1" class="layui-btn" value="重置 " lay-filter="demo1" >
                        <button class="layui-btn" name="cancel1" id="cancel1">取消</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js">
</script>
<script>
    function clearForm(form) {
        // input清空
        $(':input', form).each(function () {
            var type = this.type;
            var tag = this.tagName.toLowerCase(); // normalize case
            if (type == 'text' || type == 'password') {
                this.value = "";
            }
        })
    }
</script>
<script>
    //实现所属应用显示
    function load1(){
        $.ajax({
            url: "${pageContext.request.contextPath}/appoption",    //后台controller里的方法名称
            contentType: "application/json; charset=utf-8",
            type: "get",
            async : true ,
            dataType: "json",
            success: function (date) {
                for (var j = 0; j <date.data.length;j++) {
                    $("#app").append($("<option>").text(date.data[j].appName));
                }

            }
        });
    }
</script>
</body>


