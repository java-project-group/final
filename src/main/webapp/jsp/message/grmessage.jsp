<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>个人信息</title>
    <link rel="stylesheet" type="text/css" href="../../layui/css/layui.css">
    <link rel="stylesheet" type="text/css" href="../../layui/css/modalBox.css">
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
        个人信息
    </legend>
</fieldset>
<div style="padding: 15px;">
    <form name="frmadduser" id="frmadduser" method="post">
        <div style="padding: 15px;">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">用户名</label>
                    <div class="layui-input-inline">
                        <input type="text" name="userLoginId" Id="userLoginId" lay-verify=""  autocomplete="off" class="layui-input" value="${USER_SESSION.userLoginId}">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">姓名</label>
                    <div class="layui-input-inline">
                        <input type="text" name="userName" lay-verify=""  autocomplete="off" id="userName" class="layui-input" value=" ${USER_SESSION.userName}">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">邮箱</label>
                    <div class="layui-input-inline">
                        <input type="text" name="userEmail" lay-verify="" id="userEmail" autocomplete="off" class="layui-input" value="${USER_SESSION.userEmail}">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">联系方式</label>
                    <div class="layui-input-inline">
                        <input type="text" name="userPhone" lay-verify="" id="userPhone" autocomplete="off" class="layui-input" value="${USER_SESSION.userPhone}">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">账号类型</label>
                    <div class="layui-input-inline">
                        <input type="text" name="isSystem" lay-verify=""  autocomplete="off" id="isSystem" class="layui-input" value="${USER_SESSION.isSystem}">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js">
</script>
</html>
