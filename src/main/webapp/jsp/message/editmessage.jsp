<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>修改密码</title>
    <link rel="stylesheet" type="text/css" href="../../layui/css/layui.css">
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
        修改密码
    </legend>
</fieldset>
<!-- 内容主体区域 -->

<script src="/layui/layui.js"></script>
<table class="layui-hide" id="test" lay-filter="test">
    <form name="frmeditpassword" id="frmeditpassword" method="post">
    <div style="padding: 15px;">
        <div class="layui-form-item">
            <label class="layui-form-label">输入旧密码</label>
            <div class="layui-input-inline">
                <input type="password" name="oldpass"id="oldpass" lay-verify="pass" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid layui-word-aux">请填写6到12位密码</div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">输入新密码</label>
            <div class="layui-input-inline">
                <input type="password" name="newpass" id="newpass" lay-verify="pass"  autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid layui-word-aux">请填写6到12位密码</div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">确认新密码</label>
            <div class="layui-input-inline">
                <input type="password" name="surepass" id="surepass" lay-verify="pass"  autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid layui-word-aux">请填写6到12位密码</div>
        </div>

    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <input id="edit" name="edit" type="button" class="layui-btn" lay-submit="" lay-filter="demo1"  value="立即修改"/>
        </div>
    </div>
    </form>
</table>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js">
</script>
<script language="JavaScript">
    $('#edit').click(function () {
        var userId="${USER_SESSION.userId}";
        var old=document.getElementById('oldpass').value;
        var newpass=document.getElementById('newpass').value;
        var userPasswordId=document.getElementById('surepass').value;
        if(old=="${USER_SESSION.userPasswordId}"){
            if(newpass==userPasswordId){
                $.ajax({
                    url: "${pageContext.request.contextPath }/passwordRevise",
                    type: "post",
                    //定义回调响应的数据格式为JSON字符串,该属性可以省略
                    data:JSON.stringify(
                        {
                            userId:userId,
                            userPasswordId:userPasswordId
                        }
                    ),
                    contentType:"application/json;charset=UTF-8",
                    dataType: "json",
                    success: function (data) {
                        if (data != null) {
                            alert("提示：重置密码成功！");
                        }
                    }
                })
            }else{
                alert("提示：两次输入的密码不一样！");
            }
        }else{
            alert("提示：输入密码不正确！");
    }
    })
</script>
</body>
</html>