<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../layui/css/layui.css">
    <link rel="stylesheet" type="text/css" href="../../layui/css/modalBox.css">
</head>
<body>
<!-- 模态框 -->
<div id="myModal" class="modal1">
    <div class="modal-content1">
        <div class="modal-header1">
            <h2>添加应用</h2>
            <span id="closeBtn" class="close1">×</span>
        </div>
        <div class="modal-body1">
            <form name="frmaddapp" id="frmaddapp" method="post">
                <div style="padding: 15px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">应用名称</label>
                            <div class="layui-input-inline">
                                <input type="text" name="appName" Id="appName" lay-verify=""  autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">应用代码</label>
                            <div class="layui-input-inline">
                                <input type="text" name="appCode" lay-verify=""  autocomplete="off" id="appCode" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">应用类型</label>
                            <div class="layui-input-inline">
                                <input type="text" name="type" lay-verify="" id="type" autocomplete="off" class="layui-input" placeholder="0:普通应用,1:系统应用">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" name="btnaddbyjson" id="btnaddbyjson">添加</button>
                        <input type="button" name="reset1" id="reset1" class="layui-btn" value="重置 " lay-filter="demo1" >
                        <button class="layui-btn" name="cancel1" id="cancel1">取消</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js"></script>
<script>
    function clearForm(form) {
        // input清空
        $(':input', form).each(function () {
            var type = this.type;
            var tag = this.tagName.toLowerCase(); // normalize case
            if (type == 'text' || type == 'password' || type == 'email' || tag == 'textarea') {
                this.value = "";
            }
        })
    }
</script>
</body>


