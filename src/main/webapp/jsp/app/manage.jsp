<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../layui/css/layui.css">
    <link rel="stylesheet" type="text/css" href="../../layui/css/modalBox.css">
</head>
<body>
<!-- 模态框 -->
<div id="myModal2" class="modal1">
    <div class="modal-content1">
        <div class="modal-header1">
            <h1 >管理模块</h1>
            <span id="closeBtn3" class="close1">×</span>
        </div>
        <div class="modal-body1">
            <form name="manage1" id="manage1" method="post">
                <div  style="float: left">
                    <select id="rightbox" style="width: 400px;height: 250px;font-size: 20px;margin-left: 30px" multiple="multiple" >
                        <optgroup label="该应用所具有的模块："></optgroup>
                    </select>
                </div>
                <div class="layui-form-item" >
                    <div class="layui-input-block" style="margin-top: 50px">
                        <button class="layui-btn" id="delmodule1" onclick="delmodule()" style="margin-left: 70px ">删除模块</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<script>
    function delmodule() {
        var moduleName=$('#rightbox option:selected').text();//选中的文本
        $.ajax({
            url: "${pageContext.request.contextPath }/Delmod",
            type: "post",
            //定义回调响应的数据格式为JSON字符串,该属性可以省略
            data:JSON.stringify(
                {
                    moduleName:moduleName,
                }
            ),
            contentType:"application/json;charset=UTF-8",
            dataType: "json",
        })
    }

</script>
</html>
