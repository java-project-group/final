<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../layui/css/layui.css">
    <link rel="stylesheet" type="text/css" href="../../layui/css/modalBox.css">
</head>
<body>
<!-- 模态框 -->
<div id="myModal1" class="modal1">
    <div class="modal-content1">
        <div class="modal-header1">
            <h2>修改信息</h2>
            <span id="closeBtn2" class="close1">×</span>
        </div>
        <div class="modal-body1">
            <form name="frmeditapp" id="frmeditapp" method="post">
                <div style="padding: 15px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">应用名称</label>
                            <div class="layui-input-inline">
                                <input type="text" name="appName1" id="appName1" lay-verify=""  autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">应用代码</label>
                            <div class="layui-input-inline">
                                <input type="text" name="appCode1" id="appCode1" lay-verify=""  autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">应用类型</label>
                            <div class="layui-input-inline">
                                <input type="text" name="type1"  id="type1" lay-verify=""  autocomplete="off" class="layui-input" placeholder="输入普通应用or系统应用">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" lay-submit="" lay-filter="demo1" name="btneditbyjson" id="btneditbyjson">修改</button>
                        <input type="button" name="reset2" id="reset2" class="layui-btn" value="重置 " lay-filter="demo1" >
                        <button class="layui-btn" lay-submit="" lay-filter="demo1">取消</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script language="JavaScript">

</script>
</body>
