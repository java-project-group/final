package com.hbxy.portal.admin.right.service.impl;

import com.hbxy.portal.admin.right.mapper.PoRoleMapper;
import com.hbxy.portal.admin.right.model.PoRole;
import com.hbxy.portal.admin.right.model.PoRoleExample;
import com.hbxy.portal.admin.right.service.PoRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PoRoleServiceImpl implements PoRoleService {
    @Autowired
    private PoRoleMapper mapper;

    @Override
    public long countByExample(PoRoleExample example) {
        return mapper.countByExample(example);
    }

    @Override
    public int deleteByExample(PoRoleExample example) {
        return mapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Long roleId) {
        return mapper.deleteByPrimaryKey(roleId);
    }

    @Override
    public int insert(PoRole record) {
        return mapper.insert(record);
    }

    @Override
    public int insertSelective(PoRole record) {
        return mapper.insertSelective(record);
    }

    @Override
    public List<PoRole> selectByExample(PoRoleExample example) {
        return mapper.selectByExample(example);
    }

    @Override
    public PoRole selectByPrimaryKey(Long roleId) {
        return mapper.selectByPrimaryKey(roleId);
    }

    @Override
    public int updateByExampleSelective(PoRole record, PoRoleExample example) {
        return mapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(PoRole record, PoRoleExample example) {
        return mapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(PoRole record) {
        return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(PoRole record) {
        return mapper.updateByPrimaryKey(record);
    }
}
