package com.hbxy.portal.admin.right.service;

import com.hbxy.portal.admin.right.model.PoRole;
import com.hbxy.portal.admin.right.model.PoRoleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PoRoleService {
    long countByExample(PoRoleExample example);

    int deleteByExample(PoRoleExample example);

    int deleteByPrimaryKey(Long roleId);

    int insert(PoRole record);

    int insertSelective(PoRole record);

    List<PoRole> selectByExample(PoRoleExample example);

    PoRole selectByPrimaryKey(Long roleId);

    int updateByExampleSelective(@Param("record") PoRole record, @Param("example") PoRoleExample example);

    int updateByExample(@Param("record") PoRole record, @Param("example") PoRoleExample example);

    int updateByPrimaryKeySelective(PoRole record);

    int updateByPrimaryKey(PoRole record);
}
