package com.hbxy.portal.admin.right.mapper;

import com.hbxy.portal.admin.right.model.PoRole;
import com.hbxy.portal.admin.right.model.PoRoleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PoRoleMapper {
    long countByExample(PoRoleExample example);

    int deleteByExample(PoRoleExample example);

    int deleteByPrimaryKey(Long roleId);

    int insert(PoRole record);

    int insertSelective(PoRole record);

    List<PoRole> selectByExample(PoRoleExample example);

    PoRole selectByPrimaryKey(Long roleId);

    int updateByExampleSelective(@Param("record") PoRole record, @Param("example") PoRoleExample example);

    int updateByExample(@Param("record") PoRole record, @Param("example") PoRoleExample example);

    int updateByPrimaryKeySelective(PoRole record);

    int updateByPrimaryKey(PoRole record);
}