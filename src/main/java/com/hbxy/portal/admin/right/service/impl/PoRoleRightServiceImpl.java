package com.hbxy.portal.admin.right.service.impl;

import com.hbxy.portal.admin.right.mapper.PoRoleRightMapper;
import com.hbxy.portal.admin.right.model.PoRoleRight;
import com.hbxy.portal.admin.right.model.PoRoleRightExample;
import com.hbxy.portal.admin.right.service.PoRoleRightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PoRoleRightServiceImpl implements PoRoleRightService {
    @Autowired
    private PoRoleRightMapper mapper;

    @Override
    public long countByExample(PoRoleRightExample example) {
        return mapper.countByExample(example);
    }

    @Override
    public int deleteByExample(PoRoleRightExample example) {
        return mapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Long roleRightId) {
        return mapper.deleteByPrimaryKey(roleRightId);
    }

    @Override
    public int insert(PoRoleRight record) {
        return mapper.insert(record);
    }

    @Override
    public int insertSelective(PoRoleRight record) {
        return mapper.insertSelective(record);
    }

    @Override
    public List<PoRoleRight> selectByExample(PoRoleRightExample example) {
        return mapper.selectByExample(example);
    }

    @Override
    public PoRoleRight selectByPrimaryKey(Long roleRightId) {
        return mapper.selectByPrimaryKey(roleRightId);
    }

    @Override
    public int updateByExampleSelective(PoRoleRight record, PoRoleRightExample example) {
        return mapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(PoRoleRight record, PoRoleRightExample example) {
        return mapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(PoRoleRight record) {
        return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(PoRoleRight record) {
        return mapper.updateByPrimaryKey(record);
    }

}
