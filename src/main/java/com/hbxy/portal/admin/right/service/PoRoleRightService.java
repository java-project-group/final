package com.hbxy.portal.admin.right.service;

import com.hbxy.portal.admin.right.model.PoRoleRight;
import com.hbxy.portal.admin.right.model.PoRoleRightExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PoRoleRightService {
    long countByExample(PoRoleRightExample example);

    int deleteByExample(PoRoleRightExample example);

    int deleteByPrimaryKey(Long roleRightId);

    int insert(PoRoleRight record);

    int insertSelective(PoRoleRight record);

    List<PoRoleRight> selectByExample(PoRoleRightExample example);

    PoRoleRight selectByPrimaryKey(Long roleRightId);

    int updateByExampleSelective(@Param("record") PoRoleRight record, @Param("example") PoRoleRightExample example);

    int updateByExample(@Param("record") PoRoleRight record, @Param("example") PoRoleRightExample example);

    int updateByPrimaryKeySelective(PoRoleRight record);

    int updateByPrimaryKey(PoRoleRight record);

}


