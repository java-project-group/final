package com.hbxy.portal.admin.right.controller;



import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hbxy.portal.admin.app.model.PoModule;
import com.hbxy.portal.admin.app.model.PoModuleExample;
import com.hbxy.portal.admin.app.service.PoModuleService;
import com.hbxy.portal.admin.org.model.PoRoleUser;
import com.hbxy.portal.admin.org.model.PoRoleUserExample;
import com.hbxy.portal.admin.org.model.PoUserLogin;
import com.hbxy.portal.admin.org.model.PoUserLoginExample;
import com.hbxy.portal.admin.org.service.PoRoleUserService;
import com.hbxy.portal.admin.org.service.PoUserLoginService;
import com.hbxy.portal.admin.right.model.*;
import com.hbxy.portal.admin.right.service.PoRoleRightService;
import com.hbxy.portal.admin.right.service.PoRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class RoleController {
    @Autowired
    private PoRoleService roleService;
    @Autowired
    private PoRoleRightService roleRightService;
    @Autowired
    private PoModuleService poModuleService;
    @Autowired
    private PoRoleUserService poRoleUserService;
    @Autowired
    private PoUserLoginService poUserLoginService;
    //角色列表
    @ResponseBody
    @RequestMapping("/toRoleList")
    public JSONObject toRoleList() {
            JSONObject json = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            PoRoleExample example = new PoRoleExample();
            PoRoleExample.Criteria criteria = example.createCriteria();
            criteria.andRoleIdIsNotNull();
            List<PoRole> roleList = roleService.selectByExample(example);
            json.put("code", 0);
            json.put("msg", "");
            json.put("count", 1000);
            for (int i = 0; i < roleList.size(); i++) {
                JSONObject jsondata = new JSONObject();
                jsondata.put("roleId", roleList.get(i).getRoleId());
                jsondata.put("roleCode", roleList.get(i).getRoleCode());
                jsondata.put("roleName", roleList.get(i).getRoleName());
                jsonArray.add(jsondata);
            }
            json.put("data", jsonArray);
            return json;

    }

    //出始化添加角色，里面的right框
    @ResponseBody
    @RequestMapping("/toRoleAdd")
    public JSONObject send() {
        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        PoModuleExample example = new PoModuleExample();
        PoModuleExample.Criteria criteria = example.createCriteria();
        criteria.andModuleIdIsNotNull();
        List<PoModule> modulelist = poModuleService.selectByExample(example);
        json.put("code", 0);
        json.put("msg", "");
        json.put("count", 1000);
        for (int i = 0; i < modulelist.size(); i++) {
            JSONObject jsondata = new JSONObject();
            jsondata.put("moduleName", modulelist.get(i).getModuleName());
            jsondata.put("moduleId", modulelist.get(i).getModuleId());
            jsonArray.add(jsondata);
        }
        json.put("data", jsonArray);
        return json;
    }

    //添加角色
    @RequestMapping("/roleAdd")
    @ResponseBody
    public PoRole roleAdd(@RequestBody Map<String,String> map ,PoRole poRole,PoRoleRight poRoleRight){
        String roleName=map.get("roleName");
        String roleCode=map.get("roleCode");
        String MyString=map.get("moduleids");
        String[] Moid= MyString.split(",");
        poRole.setRoleName(roleName);
        poRole.setRoleCode(roleCode);
        //插入role表
        roleService.insert(poRole);
        //根据roleName找到roleid
        PoRoleExample example=new PoRoleExample();
        PoRoleExample.Criteria  criteria=example.createCriteria();
        criteria.andRoleNameEqualTo(map.get("roleName"));
        List<PoRole> rolelist=roleService.selectByExample(example);
        Long roleid=new Long(10);
        for(int i=0;i<rolelist.size();i++){
            roleid=rolelist.get(i).getRoleId();
        }

        Long [] a=new Long[Moid.length];
        for(int i=0;i<Moid.length;i++)
        {
                a[i]=Long.parseLong(Moid[i]);
        }
        for(int i=0;i<a.length;i++)
        {
            poRoleRight.setModuleId(a[i]);
            poRoleRight.setRoleId(roleid);
            roleRightService.insert(poRoleRight);
        }
        return  poRole;
    }

    //角色修改数据回填，已分配权限左边框
    @RequestMapping("/roleright")
    @ResponseBody
    public  JSONObject manage(@RequestBody PoRole poRole){
        JSONObject json = new JSONObject();
        JSONArray jsonArray=new JSONArray();
        PoRoleRightExample example = new PoRoleRightExample();
        PoRoleRightExample.Criteria criteria=example.createCriteria();
        criteria.andRoleIdEqualTo(poRole.getRoleId());
        List<PoRoleRight> rolerightlist=roleRightService.selectByExample(example);

        json.put("code", 0);
        json.put("msg", "");
        json.put("count", 1000);
        for (int i = 0; i < rolerightlist.size(); i++) {
            JSONObject jsondata = new JSONObject();
            jsondata.put("moduleId", rolerightlist.get(i).getModuleId());
            PoModuleExample example1 = new PoModuleExample();
            PoModuleExample.Criteria criteria1=example1.createCriteria();
            criteria1.andModuleIdEqualTo(rolerightlist.get(i).getModuleId());
            List<PoModule> modulename=poModuleService.selectByExample(example1);
            jsondata.put("moduleName", modulename.get(0).getModuleName());
            jsonArray.add(jsondata);
        }
        json.put("data",jsonArray);
        return  json;
    }

    //修改角色
    @RequestMapping("/roleEdit")
    @ResponseBody
    public PoRole roleEdit(@RequestBody Map<String,String> map ,PoRole poRole,PoRoleRight poRoleRight){
        String roleName=map.get("roleName");
        String roleCode=map.get("roleCode");
        Long roleid=Long.parseLong(map.get("roleId"));
        String MyString=map.get("moduleids");
        String[] Moid= MyString.split(",");
        poRole.setRoleId(roleid);
        poRole.setRoleName(roleName);
        poRole.setRoleCode(roleCode);
        //修改role表
        roleService.updateByPrimaryKeySelective(poRole);
        //根据roleName找到roleid
        PoRoleRightExample example = new PoRoleRightExample();
        PoRoleRightExample.Criteria criteria=example.createCriteria();
        criteria.andRoleIdEqualTo(roleid);
        List<PoRoleRight> rolerightlist=roleRightService.selectByExample(example);
        for(int i=0;i<rolerightlist.size();i++){
            roleRightService.deleteByPrimaryKey(rolerightlist.get(i).getRoleRightId());
        }
        Long [] a=new Long[Moid.length];
        for(int i=0;i<Moid.length;i++)
        {
            a[i]=Long.parseLong(Moid[i]);
        }
        for(int i=0;i<a.length;i++)
        {
            poRoleRight.setModuleId(a[i]);
            poRoleRight.setRoleId(roleid);
            roleRightService.insert(poRoleRight);
        }

        return  poRole;
    }
    //用户角色数据回填
    @RequestMapping("/roleuser")
    @ResponseBody
    public  JSONObject man(@RequestBody PoUserLogin poUserLogin){
        JSONObject json = new JSONObject();
        JSONArray jsonArray=new JSONArray();
        PoRoleUserExample example = new PoRoleUserExample();
        PoRoleUserExample.Criteria criteria=example.createCriteria();
        criteria.andUserIdEqualTo(poUserLogin.getUserId());
        List<PoRoleUser> roleuserlist=poRoleUserService.selectByExample(example);

        json.put("code", 0);
        json.put("msg", "");
        json.put("count", 1000);
        for (int i = 0; i < roleuserlist.size(); i++) {
            JSONObject jsondata = new JSONObject();
            jsondata.put("roleId", roleuserlist.get(i).getRoleId());
            PoRoleExample example1 = new PoRoleExample();
            PoRoleExample.Criteria criteria1=example1.createCriteria();
            criteria1.andRoleIdEqualTo(roleuserlist.get(i).getRoleId());
            List<PoRole> rolename=roleService.selectByExample(example1);
            jsondata.put("roleName", rolename.get(0).getRoleName());
            jsonArray.add(jsondata);
        }
        json.put("data",jsonArray);
        return  json;
    }
    //出始化人员授权，里面的right框
    @ResponseBody
    @RequestMapping("/toRoleuser")
    public JSONObject sendb() {
        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        PoRoleExample example = new PoRoleExample();
        PoRoleExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdIsNotNull();
        List<PoRole> rolelist = roleService.selectByExample(example);
        json.put("code", 0);
        json.put("msg", "");
        json.put("count", 1000);
        for (int i = 0; i < rolelist.size(); i++) {
            JSONObject jsondata = new JSONObject();
            jsondata.put("roleName", rolelist.get(i).getRoleName());
            jsondata.put("roleId", rolelist.get(i).getRoleId());
            jsonArray.add(jsondata);
        }
        json.put("data", jsonArray);
        return json;
    }
    //修改人员权限
    @RequestMapping("/roleuserEdit")
    @ResponseBody
    public PoUserLogin roleuserEdit(@RequestBody Map<String,String> map ,PoUserLogin poUserLogin,PoRoleUser poRoleUser){

        Long userid=Long.parseLong(map.get("userId"));
        String MyString=map.get("roleids");
        String[] Moid= MyString.split(",");
        //根据roleName找到roleid
        PoRoleUserExample example = new PoRoleUserExample();
        PoRoleUserExample.Criteria criteria=example.createCriteria();
        criteria.andUserIdEqualTo(userid);
        List<PoRoleUser> roleuserlist=poRoleUserService.selectByExample(example);
        for(int i=0;i<roleuserlist.size();i++){
            poRoleUserService.deleteByPrimaryKey(roleuserlist.get(i).getRoleUserId());
        }
        Long [] a=new Long[Moid.length];
        for(int i=0;i<Moid.length;i++)
        {
            a[i]=Long.parseLong(Moid[i]);
        }
        for(int i=0;i<a.length;i++)
        {
            poRoleUser.setRoleId(a[i]);
            poRoleUser.setUserId(userid);
            poRoleUserService.insert(poRoleUser);
        }

        return  poUserLogin;
    }

    //删除角色
    @RequestMapping("/roleDel")
    @ResponseBody
    public PoRole roleDel(@RequestBody PoRole role){
        //删除角色
        roleService.deleteByPrimaryKey(role.getRoleId());
        //删除角色模块关系
        PoRoleRightExample rightExample = new PoRoleRightExample();
        rightExample.createCriteria().andRoleIdEqualTo(role.getRoleId());
        roleRightService.deleteByExample(rightExample);
        //删除用户角色关系
        PoRoleUserExample example = new PoRoleUserExample ();
        PoRoleUserExample.Criteria criteria=example.createCriteria();
        criteria.andRoleIdEqualTo(role.getRoleId());
        poRoleUserService.deleteByExample(example);
        return  role;
    }
    //遍历出授权列表
    @ResponseBody
    @RequestMapping("/toRoleUserList")
    public JSONObject sendMemTable() {
        //json 对象，使用大括号{ }，如{key：value}，就是一种特殊格式的map，存放键值对
        JSONObject json = new JSONObject();
        // JSONArray对象，是JSON数组格式，存放的是一个或者多个JSONObject对象
        JSONArray jsonArray=new JSONArray();
        //实例化这个类
        PoUserLoginExample example=new PoUserLoginExample();
        //Criteria中可以增加查询条件
        PoUserLoginExample.Criteria criteria=example.createCriteria();
        criteria.andUserIdIsNotNull();
        List<PoUserLogin>userlist=poUserLoginService.selectByExample(example);
        json.put("code", 0);
        json.put("msg", "");
        //最多能显示多少行
        json.put("count", 1000);
        for (int i = 0; i < userlist.size(); i++) {
            String rolenames="";
            JSONObject jsondata = new JSONObject();
            jsondata.put("userId", userlist.get(i).getUserId());
            jsondata.put("userLoginId", userlist.get(i).getUserLoginId());
            jsondata.put("userName", userlist.get(i).getUserName());
            PoRoleUserExample example1=new PoRoleUserExample();
            //Criteria中可以增加查询条件
            PoRoleUserExample.Criteria criteria1=example1.createCriteria();
            criteria1.andUserIdEqualTo(userlist.get(i).getUserId());
            List<PoRoleUser>roleuserlist=poRoleUserService.selectByExample(example1);
            for(int j=0;j<roleuserlist.size();j++){
                PoRoleExample example2=new PoRoleExample();
                PoRoleExample.Criteria criteria2=example2.createCriteria();
                criteria2.andRoleIdEqualTo(roleuserlist.get(j).getRoleId());
                List<PoRole>rolelist=roleService.selectByExample(example2);
                rolenames+=rolelist.get(0).getRoleName()+" ";
            }
            jsondata.put("rolenames",rolenames);
            //显示的时候不显示0或者1
            jsondata.put("isSystem",userlist.get(i).getIsSystem().contains("0")?"系统管理员":"普通用户");
            jsonArray.add(jsondata);
        }
        json.put("data",jsonArray);
        return json;
    }
}

