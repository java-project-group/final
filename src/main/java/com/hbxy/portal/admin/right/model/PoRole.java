package com.hbxy.portal.admin.right.model;

import java.io.Serializable;

/**
 * po_role
 * @author 
 */
public class PoRole implements Serializable {
    private Long roleId;

    private String roleName;

    private String roleCode;

    private static final long serialVersionUID = 1L;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
}