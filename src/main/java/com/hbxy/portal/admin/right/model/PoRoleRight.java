package com.hbxy.portal.admin.right.model;

import java.io.Serializable;

/**
 * po_role_right
 * @author 
 */
public class PoRoleRight implements Serializable {
    private Long roleRightId;

    private Long roleId;

    private Long moduleId;

    private static final long serialVersionUID = 1L;

    public Long getRoleRightId() {
        return roleRightId;
    }

    public void setRoleRightId(Long roleRightId) {
        this.roleRightId = roleRightId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }
}