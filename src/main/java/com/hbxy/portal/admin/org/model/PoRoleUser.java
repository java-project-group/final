package com.hbxy.portal.admin.org.model;

import java.io.Serializable;

/**
 * po_role_user
 * @author 
 */
public class PoRoleUser implements Serializable {
    private Long roleUserId;

    private Long roleId;

    private Long userId;

    private static final long serialVersionUID = 1L;

    public Long getRoleUserId() {
        return roleUserId;
    }

    public void setRoleUserId(Long roleUserId) {
        this.roleUserId = roleUserId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}