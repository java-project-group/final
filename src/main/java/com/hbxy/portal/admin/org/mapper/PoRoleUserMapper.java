package com.hbxy.portal.admin.org.mapper;

import com.hbxy.portal.admin.org.model.PoRoleUser;
import com.hbxy.portal.admin.org.model.PoRoleUserExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PoRoleUserMapper {
    long countByExample(PoRoleUserExample example);

    int deleteByExample(PoRoleUserExample example);

    int deleteByPrimaryKey(Long roleUserId);

    int insert(PoRoleUser record);

    int insertSelective(PoRoleUser record);

    List<PoRoleUser> selectByExample(PoRoleUserExample example);

    PoRoleUser selectByPrimaryKey(Long roleUserId);

    int updateByExampleSelective(@Param("record") PoRoleUser record, @Param("example") PoRoleUserExample example);

    int updateByExample(@Param("record") PoRoleUser record, @Param("example") PoRoleUserExample example);

    int updateByPrimaryKeySelective(PoRoleUser record);

    int updateByPrimaryKey(PoRoleUser record);
}