package com.hbxy.portal.admin.org.service.impl;

import com.hbxy.portal.admin.org.mapper.PoUserLoginMapper;
import com.hbxy.portal.admin.org.model.PoUserLogin;
import com.hbxy.portal.admin.org.model.PoUserLoginExample;
import com.hbxy.portal.admin.org.service.PoUserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PoUserLoginServiceImpl implements PoUserLoginService {

    @Autowired
    private PoUserLoginMapper userLoginMapper;

    @Override
    public long countByExample(PoUserLoginExample example) {
        return userLoginMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(PoUserLoginExample example) {
        return userLoginMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Long partyId) {
        return userLoginMapper.deleteByPrimaryKey(partyId);
    }

    @Override
    public int insert(PoUserLogin record) {
        return userLoginMapper.insert(record);
    }

    @Override
    public int insertSelective(PoUserLogin record) {
        return userLoginMapper.insertSelective(record);
    }

    @Override
    public List<PoUserLogin> selectByExample(PoUserLoginExample example) {
        return userLoginMapper.selectByExample(example);
    }

    @Override
    public PoUserLogin selectByPrimaryKey(Long partyId) {
        return userLoginMapper.selectByPrimaryKey(partyId);
    }

    @Override
    public int updateByExampleSelective(PoUserLogin record, PoUserLoginExample example) {
        return userLoginMapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateByExample(PoUserLogin record, PoUserLoginExample example) {
        return userLoginMapper.updateByExample(record, example);
    }

    @Override
    public int updateByPrimaryKeySelective(PoUserLogin record) {
        return userLoginMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(PoUserLogin record) {
        return userLoginMapper.updateByPrimaryKey(record);
    }

    @Override
    public PoUserLogin findByLoginId(String userLoginId) {
        PoUserLogin userLogin = null;
        //实例化这个类
        PoUserLoginExample example = new PoUserLoginExample();
        //Criteria中可以增加查询条件
        example.createCriteria().andUserLoginIdEqualTo(userLoginId);
        List<PoUserLogin> userLogins = userLoginMapper.selectByExample(example);
        //1 判断登录账号
        if (null != userLogins && 0 != userLogins.size()) {
            userLogin = userLogins.get(0);
        }
        return userLogin;
    }
}