package com.hbxy.portal.admin.org.service;

import com.hbxy.portal.admin.org.model.PoUserLogin;
import com.hbxy.portal.admin.org.model.PoUserLoginExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PoUserLoginService {
    long countByExample(PoUserLoginExample example);

    int deleteByExample(PoUserLoginExample example);

    int deleteByPrimaryKey(Long partyId);

    int insert(PoUserLogin record);

    int insertSelective(PoUserLogin record);

    List<PoUserLogin> selectByExample(PoUserLoginExample example);

    PoUserLogin selectByPrimaryKey(Long partyId);

    int updateByExampleSelective(@Param("record") PoUserLogin record, @Param("example") PoUserLoginExample example);

    int updateByExample(@Param("record") PoUserLogin record, @Param("example") PoUserLoginExample example);

    int updateByPrimaryKeySelective(PoUserLogin record);

    int updateByPrimaryKey(PoUserLogin record);

    public PoUserLogin findByLoginId(String userLoginId);




}
