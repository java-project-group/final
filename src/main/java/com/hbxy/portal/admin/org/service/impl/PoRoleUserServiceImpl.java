package com.hbxy.portal.admin.org.service.impl;

import com.hbxy.portal.admin.org.mapper.PoRoleUserMapper;
import com.hbxy.portal.admin.org.model.PoRoleUser;
import com.hbxy.portal.admin.org.model.PoRoleUserExample;
import com.hbxy.portal.admin.org.service.PoRoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PoRoleUserServiceImpl implements PoRoleUserService {
   @Autowired
    private PoRoleUserMapper roleUserMapper;
    @Override
    public long countByExample(PoRoleUserExample example) {
        return roleUserMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(PoRoleUserExample example) {
            return roleUserMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Long roleUserId) {
        return roleUserMapper.deleteByPrimaryKey(roleUserId);
    }

    @Override
    public int insert(PoRoleUser record) {
        return roleUserMapper.insert(record);
    }

    @Override
    public int insertSelective(PoRoleUser record) {
        return roleUserMapper.insertSelective(record);
    }

    @Override
    public List<PoRoleUser> selectByExample(PoRoleUserExample example) {
        return roleUserMapper.selectByExample(example);
    }

    @Override
    public PoRoleUser selectByPrimaryKey(Long roleUserId) {
        return roleUserMapper.selectByPrimaryKey(roleUserId);
    }

    @Override
    public int updateByExampleSelective(PoRoleUser record, PoRoleUserExample example) {
        return roleUserMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(PoRoleUser record, PoRoleUserExample example) {
        return roleUserMapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(PoRoleUser record) {
        return roleUserMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(PoRoleUser record) {
        return roleUserMapper.updateByPrimaryKey(record);
    }
}
