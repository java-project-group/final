package com.hbxy.portal.admin.org.mapper;

import com.hbxy.portal.admin.org.model.PoUserLogin;
import com.hbxy.portal.admin.org.model.PoUserLoginExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PoUserLoginMapper {
    long countByExample(PoUserLoginExample example);

    int deleteByExample(PoUserLoginExample example);

    int deleteByPrimaryKey(Long userId);

    int insert(PoUserLogin record);

    int insertSelective(PoUserLogin record);

    List<PoUserLogin> selectByExample(PoUserLoginExample example);

    PoUserLogin selectByPrimaryKey(Long userId);

    int updateByExampleSelective(@Param("record") PoUserLogin record, @Param("example") PoUserLoginExample example);

    int updateByExample(@Param("record") PoUserLogin record, @Param("example") PoUserLoginExample example);

    int updateByPrimaryKeySelective(PoUserLogin record);

    int updateByPrimaryKey(PoUserLogin record);

}