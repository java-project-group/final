package com.hbxy.portal.admin.org.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hbxy.portal.admin.org.model.*;
import com.hbxy.portal.admin.org.service.PoRoleUserService;
import com.hbxy.portal.admin.org.service.PoUserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
public class UserLoginController {
    //@ResponseBody用来返回JSON数据，@RequestBody 将前台发送过来的json封装为对应的 JavaBean 对象
    @Autowired
    PoUserLoginService userLoginService;
    @Autowired
    PoRoleUserService roleUserService;

    //打印所有用户信息
    @ResponseBody
    @RequestMapping("/toUserList")
        public JSONObject sendMemTable() {
           //json 对象，使用大括号{ }，如{key：value}，就是一种特殊格式的map，存放键值对
            JSONObject json = new JSONObject();
            // JSONArray对象，是JSON数组格式，存放的是一个或者多个JSONObject对象
            JSONArray jsonArray=new JSONArray();
            //实例化这个类
            PoUserLoginExample example=new PoUserLoginExample();
            //Criteria中可以增加查询条件
            PoUserLoginExample.Criteria criteria=example.createCriteria();
            criteria.andUserIdIsNotNull();
            List<PoUserLogin>userlist=userLoginService.selectByExample(example);
            json.put("code", 0);
            json.put("msg", "");
            //最多能显示多少行
            json.put("count", 1000);
            for (int i = 0; i < userlist.size(); i++) {
                JSONObject jsondata = new JSONObject();
                jsondata.put("userId", userlist.get(i).getUserId());
                jsondata.put("userLoginId", userlist.get(i).getUserLoginId());
                jsondata.put("userName", userlist.get(i).getUserName());
                jsondata.put("userPhone", userlist.get(i).getUserPhone());
                jsondata.put("userEmail", userlist.get(i).getUserEmail());
                //显示的时候不显示0或者1
                jsondata.put("isSystem",userlist.get(i).getIsSystem().contains("0")?"系统管理员":"普通用户");
                jsonArray.add(jsondata);
            }
            json.put("data",jsonArray);
            return json;
        }
    //添加用户
    @RequestMapping("/userAdd")
    @ResponseBody
    public PoUserLogin userAdd(@RequestBody PoUserLogin user){
        userLoginService.insert(user);
        return  user;
    }

    //修改用户信息
    @RequestMapping("/userEdit")
    @ResponseBody
    public PoUserLogin userEdit(@RequestBody PoUserLogin user){
        userLoginService.updateByPrimaryKeySelective(user);
        return  user;
    }
    //删除用户
    @RequestMapping("/userDel")
    @ResponseBody
    public PoUserLogin userDel(@RequestBody PoUserLogin user){
        //删除用户
        userLoginService.deleteByPrimaryKey(user.getUserId());
        //删除用户角色关系
        PoRoleUserExample example = new PoRoleUserExample ();
        PoRoleUserExample.Criteria criteria=example.createCriteria();
        criteria.andUserIdEqualTo(user.getUserId());
        roleUserService.deleteByExample(example);
        return  user;
    }
    //修改用户密码
    @RequestMapping("/passwordRevise")
    @ResponseBody
    public PoUserLogin  passwordRevise(@RequestBody PoUserLogin user){
      userLoginService.updateByPrimaryKeySelective(user);
        return  user;
    }

}



