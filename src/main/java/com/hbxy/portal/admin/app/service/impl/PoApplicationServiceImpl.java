package com.hbxy.portal.admin.app.service.impl;

import com.hbxy.portal.admin.app.mapper.PoApplicationMapper;
import com.hbxy.portal.admin.app.model.PoApplication;
import com.hbxy.portal.admin.app.model.PoApplicationExample;
import com.hbxy.portal.admin.app.service.PoApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PoApplicationServiceImpl implements PoApplicationService {

    @Autowired
    PoApplicationMapper applicationMapper;

    @Override
    public long countByExample(PoApplicationExample example) {
        return applicationMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(PoApplicationExample example) {
        return applicationMapper.deleteByExample(example);
    }


    @Override
    public int deleteByPrimaryKey(Long appId) {
        return applicationMapper.deleteByPrimaryKey(appId);
    }

    @Override
    public int insert(PoApplication record) {
        return applicationMapper.insert(record);
    }

    @Override
    public int insertSelective(PoApplication record) {
        return applicationMapper.insertSelective(record);
    }

    @Override
    public List<PoApplication> selectByExample(PoApplicationExample example) {
        return applicationMapper.selectByExample(example);
    }


    @Override
    public PoApplication selectByPrimaryKey(Long appId) {
        return applicationMapper.selectByPrimaryKey(appId);
    }

    @Override
    public int updateByExampleSelective(PoApplication record, PoApplicationExample example) {
        return applicationMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(PoApplication record, PoApplicationExample example) {
        return applicationMapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(PoApplication record) {
        return applicationMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(PoApplication record) {
        return applicationMapper.updateByPrimaryKey(record);
    }
}
