package com.hbxy.portal.admin.app.model;

import java.io.Serializable;

/**
 * po_module
 * @author 
 */
public class PoModule implements Serializable {
    private Long moduleId;

    private Long appId;

    private String appName;

    private String moduleName;

    private String moduleCode;

    private static final long serialVersionUID = 1L;

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }
}