package com.hbxy.portal.admin.app.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hbxy.portal.admin.app.model.*;
import com.hbxy.portal.admin.app.service.PoApplicationService;
import com.hbxy.portal.admin.app.service.PoModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ApplicationController {
    //@ResponseBody用来返回JSON数据，@RequestBody 将前台发送过来的json封装为对应的 JavaBean 对象
    @Autowired
    PoApplicationService poApplicationService;
    @Autowired
    PoModuleService moduleService;
    //打印所有应用信息
    @ResponseBody
    @RequestMapping("/toAppList")
    public JSONObject sendMemTable() {
        //json 对象，使用大括号{ }，如{key：value}，就是一种特殊格式的map，存放键值对
        JSONObject json = new JSONObject();
        // JSONArray对象，是JSON数组格式，存放的是一个或者多个JSONObject对象
        JSONArray jsonArray=new JSONArray();
        //实例化这个类
        PoApplicationExample example=new PoApplicationExample();
        PoApplicationExample.Criteria  criteria=example.createCriteria();
        //Criteria中可以增加查询条件
        criteria.andAppIdIsNotNull();
        List<PoApplication> applist=poApplicationService.selectByExample(example);
        json.put("code", 0);
        json.put("msg", "");
        json.put("count", 1000);
        for (int i = 0; i < applist.size(); i++) {
            JSONObject jsondata = new JSONObject();
            jsondata.put("appId", applist.get(i).getAppId());
            jsondata.put("appName", applist.get(i).getAppName());
            jsondata.put("appCode", applist.get(i).getAppCode());
            jsondata.put("type", applist.get(i).getType().contains("0")?"普通应用":"系统应用");
            jsonArray.add(jsondata);
        }
        json.put("data",jsonArray);
        return json;
    }
    //添加应用
    @RequestMapping("/appAdd")
    @ResponseBody
    public PoApplication appAdd(@RequestBody PoApplication app, Model model){
        poApplicationService.insert(app);
        return  app;
    }
    //修改应用信息
    @RequestMapping("/appEdit")
    @ResponseBody
    public  PoApplication appEdit(@RequestBody PoApplication app){
        poApplicationService.updateByPrimaryKeySelective(app);
         return  app;
    }
    //删除应用
    @RequestMapping("/appDel")
    @ResponseBody
    public  PoApplication appDel(@RequestBody PoApplication app){
        //应用删除
        poApplicationService.deleteByPrimaryKey(app.getAppId());
        //模块删除
        PoModuleExample moduleExample = new PoModuleExample();
        moduleExample.createCriteria().andAppIdEqualTo(app.getAppId());
        moduleService.deleteByExample(moduleExample);
        return  app;
    }
    //根据模块id获取应用的模块
    @RequestMapping("/managemodule")
    @ResponseBody
    public  JSONObject manage(@RequestBody PoApplication app){
        JSONObject json = new JSONObject();
        JSONArray jsonArray=new JSONArray();
        PoModuleExample moduleExample = new PoModuleExample();
        PoModuleExample.Criteria criteria=moduleExample.createCriteria();
        criteria.andAppIdEqualTo(app.getAppId());
        List<PoModule> modulelist=moduleService.selectByExample(moduleExample);
        json.put("code", 0);
        json.put("msg", "");
        json.put("count", 1000);
        for (int i = 0; i < modulelist.size(); i++) {
            JSONObject jsondata = new JSONObject();
            jsondata.put("moduleId", modulelist.get(i).getModuleId());
            jsondata.put("moduleName", modulelist.get(i).getModuleName());
            jsondata.put("moduleCode", modulelist.get(i).getModuleCode());
            jsonArray.add(jsondata);
        }
        json.put("data",jsonArray);
        return  json;
    }
    //删除选中的模块
    @RequestMapping("/Delmod")
    @ResponseBody
    public PoModule moduledel(@RequestBody PoModule module) {
        PoModuleExample example = new PoModuleExample();
        PoModuleExample.Criteria criteria = example.createCriteria();
        criteria.andModuleNameEqualTo(module.getModuleName());
        moduleService.deleteByExample(example);
        return module;
    }

}
