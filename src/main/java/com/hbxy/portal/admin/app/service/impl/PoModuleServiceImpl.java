package com.hbxy.portal.admin.app.service.impl;

import com.hbxy.portal.admin.app.mapper.PoModuleMapper;
import com.hbxy.portal.admin.app.model.PoModule;
import com.hbxy.portal.admin.app.model.PoModuleExample;
import com.hbxy.portal.admin.app.service.PoModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PoModuleServiceImpl implements PoModuleService {
    @Autowired
    PoModuleMapper moduleMapper;

    @Override
    public long countByExample(PoModuleExample example) {
        return moduleMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(PoModuleExample example) {
        return moduleMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Long moduleId) {
        return moduleMapper.deleteByPrimaryKey(moduleId);
    }

    @Override
    public int insert(PoModule record) {
        return moduleMapper.insert(record);
    }

    @Override
    public int insertSelective(PoModule record) {
        return moduleMapper.insertSelective(record);
    }

    @Override
    public List<PoModule> selectByExample(PoModuleExample example) {
        return moduleMapper.selectByExample(example);
    }

    @Override
    public PoModule selectByPrimaryKey(Long moduleId) {
        return moduleMapper.selectByPrimaryKey(moduleId);
    }

    @Override
    public int updateByExampleSelective(PoModule record, PoModuleExample example) {
        return moduleMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(PoModule record, PoModuleExample example) {
        return moduleMapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(PoModule record) {
        return moduleMapper.updateByPrimaryKeySelective(record);

    }

    @Override
    public int updateByPrimaryKey(PoModule record) {
        return moduleMapper.updateByPrimaryKey(record);
    }
}
