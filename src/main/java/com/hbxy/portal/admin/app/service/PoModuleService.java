package com.hbxy.portal.admin.app.service;
 import com.hbxy.portal.admin.app.model.PoModule;
 import com.hbxy.portal.admin.app.model.PoModuleExample;
 import org.apache.ibatis.annotations.Param;
 import java.util.List;

public interface PoModuleService {
    long countByExample(PoModuleExample example);

    int deleteByExample(PoModuleExample example);

    int deleteByPrimaryKey(Long moduleId);

    int insert(PoModule record);

    int insertSelective(PoModule record);

    List<PoModule> selectByExample(PoModuleExample example);

    PoModule selectByPrimaryKey(Long moduleId);

    int updateByExampleSelective(@Param("record") PoModule record, @Param("example") PoModuleExample example);

    int updateByExample(@Param("record") PoModule record, @Param("example") PoModuleExample example);

    int updateByPrimaryKeySelective(PoModule record);

    int updateByPrimaryKey(PoModule record);

}
