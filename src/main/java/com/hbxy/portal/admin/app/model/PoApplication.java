package com.hbxy.portal.admin.app.model;

import java.io.Serializable;

/**
 * po_application
 * @author 
 */
public class PoApplication implements Serializable {
    private Long appId;

    private String appName;

    private String appCode;

    /**
     * TYPE：0-普通应用；1-系统应用
     */
    private String type;

    private static final long serialVersionUID = 1L;

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}