package com.hbxy.portal.admin.app.service;

import com.hbxy.portal.admin.app.model.PoApplication;
import com.hbxy.portal.admin.app.model.PoApplicationExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PoApplicationService {
    long countByExample(PoApplicationExample example);

    int deleteByExample(PoApplicationExample example);

    int deleteByPrimaryKey(Long appId);

    int insert(PoApplication record);

    int insertSelective(PoApplication record);

    List<PoApplication> selectByExample(PoApplicationExample example);

    PoApplication selectByPrimaryKey(Long appId);

    int updateByExampleSelective(@Param("record") PoApplication record, @Param("example") PoApplicationExample example);

    int updateByExample(@Param("record") PoApplication record, @Param("example") PoApplicationExample example);

    int updateByPrimaryKeySelective(PoApplication record);

    int updateByPrimaryKey(PoApplication record);

}
