package com.hbxy.portal.admin.app.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hbxy.portal.admin.app.model.*;
import com.hbxy.portal.admin.app.service.PoApplicationService;
import com.hbxy.portal.admin.app.service.PoModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ModuleController {
    //@ResponseBody用来返回JSON数据，@RequestBody 将前台发送过来的json封装为对应的 JavaBean 对象
    @Autowired
    PoModuleService poModuleService;
    @Autowired
    PoApplicationService poApplicationService;

    //打印所有模块信息
    @ResponseBody
    @RequestMapping("/toModuleList")
    public JSONObject sendMemTable() {
        //技术支持：yantianhao
        //json 对象，使用大括号{ }，如{key：value}，就是一种特殊格式的map，存放键值对
        JSONObject json = new JSONObject();
        // JSONArray对象，是JSON数组格式，存放的是一个或者多个JSONObject对象
        JSONArray jsonArray = new JSONArray();
        //实例化这个类
        PoModuleExample example = new PoModuleExample();
        //Criteria中可以增加查询条件
        PoModuleExample.Criteria criteria = example.createCriteria();
        criteria.andModuleIdIsNotNull();
        List<PoModule> modulelist = poModuleService.selectByExample(example);
        json.put("code", 0);
        json.put("msg", "");
        json.put("count", 1000);
        for (int i = 0; i < modulelist.size(); i++) {
            JSONObject jsondata = new JSONObject();
            jsondata.put("moduleId", modulelist.get(i).getModuleId());
            jsondata.put("moduleName", modulelist.get(i).getModuleName());
            jsondata.put("moduleCode", modulelist.get(i).getModuleCode());
            jsondata.put("appName", modulelist.get(i).getAppName());
            jsonArray.add(jsondata);
        }
        json.put("data", jsonArray);
        return json;
    }

    //添加模块
    @RequestMapping("/moduleAdd")
    @ResponseBody
    public PoModule modulepAdd(@RequestBody PoModule module) {
        //实例化
        PoApplicationExample example=new PoApplicationExample();
        PoApplicationExample.Criteria  criteria=example.createCriteria();
        criteria.andAppNameEqualTo(module.getAppName());
        List<PoApplication> applist=poApplicationService.selectByExample(example);
        //通过找到的获得appid
        Long appid=new Long(10);
        appid=applist.get(0).getAppId();
        module.setAppId(appid);
        poModuleService.insert(module);
        return module;
    }

    //修改应用
    @RequestMapping("/moduleEdit")
    @ResponseBody
    public PoModule moduleEdit(@RequestBody PoModule module) {
        poModuleService.updateByPrimaryKeySelective(module);
        return module;
    }

    //删除模块应用
    @RequestMapping("/modulerDel")
    @ResponseBody
    public PoModule moduledel(@RequestBody PoModule module) {
        poModuleService.deleteByPrimaryKey(module.getModuleId());
        return module;
    }
    //获取应用名称用来写在列表框
    @RequestMapping("/appoption")
    @ResponseBody
    public JSONObject sendoption() {
        JSONObject json = new JSONObject();
        JSONArray jsonArray=new JSONArray();
        PoApplicationExample example=new PoApplicationExample();
        PoApplicationExample.Criteria  criteria=example.createCriteria();
        criteria.andAppIdIsNotNull();
        List<PoApplication> applist=poApplicationService.selectByExample(example);
        json.put("code", 0);
        json.put("msg", "");
        json.put("count", 1000);
        for (int i = 0; i < applist.size(); i++) {
            JSONObject jsondata = new JSONObject();
            jsondata.put("appName", applist.get(i).getAppName());
            jsonArray.add(jsondata);
        }
        json.put("data",jsonArray);
        return json;
    }
}
