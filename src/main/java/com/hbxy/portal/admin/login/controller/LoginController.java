package com.hbxy.portal.admin.login.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hbxy.portal.admin.app.model.PoModule;
import com.hbxy.portal.admin.app.model.PoModuleExample;
import com.hbxy.portal.admin.app.service.PoApplicationService;
import com.hbxy.portal.admin.app.service.PoModuleService;
import com.hbxy.portal.admin.org.model.PoRoleUser;
import com.hbxy.portal.admin.org.model.PoRoleUserExample;
import com.hbxy.portal.admin.org.model.PoUserLogin;
import com.hbxy.portal.admin.org.model.PoUserLoginExample;
import com.hbxy.portal.admin.org.service.PoRoleUserService;
import com.hbxy.portal.admin.org.service.PoUserLoginService;
import com.hbxy.portal.admin.right.model.PoRole;
import com.hbxy.portal.admin.right.model.PoRoleExample;
import com.hbxy.portal.admin.right.model.PoRoleRight;
import com.hbxy.portal.admin.right.model.PoRoleRightExample;
import com.hbxy.portal.admin.right.service.PoRoleRightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;


@Controller
public class LoginController {
    @Autowired
    PoModuleService poModuleService;
    @Autowired
    PoRoleRightService poRoleRightService;
    @Autowired
    PoRoleUserService poRoleUserService;
    @Autowired
    PoUserLoginService userLoginService;
    //跳转到用户登录页面 /jsp/login.jsp
    @RequestMapping(value="/loginText",method= RequestMethod.GET)
    public  String toLogin(){
        return  "jsp/login";
    }
    // 用户登录，只处理用 POST 方法的提交
    @RequestMapping(value = "/loginText",method = RequestMethod.POST)
    public String login(PoUserLogin user, Model model, HttpSession session){
        //获取用户名和密码
        String userLoginId=user.getUserLoginId();
        String password=user.getUserPasswordId();
        //判断账号密码对不对
        if(userLoginId!=null  && password != null){
            PoUserLogin userLogin = userLoginService.findByLoginId(userLoginId);
            if (userLogin != null && password.equals(userLogin.getUserPasswordId()) ) {
                // 鉴权不用管!!//判断该用户所具有的权限
                //        JSONObject json = new JSONObject();
                //        JSONArray jsonArray = new JSONArray();
                //        PoUserLoginExample example = new PoUserLoginExample();
                //        PoUserLoginExample.Criteria criteria = example.createCriteria();
                //        criteria.andUserLoginIdEqualTo(userLoginId);
                //        (userLoginService.selectByExample(example)).get(0).getUserId();         //找到userid
                //        //跟句userid查询角色id
                //        PoRoleUserExample example1 = new PoRoleUserExample();
                //        PoRoleUserExample.Criteria criteria1 = example1.createCriteria();
                //        criteria1.andUserIdEqualTo((userLoginService.selectByExample(example)).get(0).getUserId());
                //        List<PoRoleUser> roleuserList=poRoleUserService.selectByExample(example1);
                //        //获取所拥有的模块id
                //        String moduleids="";
                //        List<PoRoleRight> roleRights;
                //        String modulename="";
                //        for(int i=0;i<roleuserList.size();i++){
                //            PoRoleRightExample example2=new PoRoleRightExample();
                //            PoRoleRightExample.Criteria criteria2=example2.createCriteria();
                //            criteria2.andRoleIdEqualTo(roleuserList.get(i).getRoleId());
                //            roleRights=poRoleRightService.selectByExample(example2);
                //              for(int y=0;y<roleRights.size();y++){
                //                   PoModuleExample example4=new PoModuleExample();
                //                   PoModuleExample.Criteria criteria4=example4.createCriteria();
                //                   criteria4.andModuleIdEqualTo(roleRights.get(y).getModuleId());
                //                  List<PoModule> modulenames=poModuleService.selectByExample(example4);
                //                  modulename+=modulenames.get(0).getModuleName()+",";
                //              }
                //        }
                //               // 登录成功，将用户对象添加到 Session
                //               session.setAttribute("USER_MODULE",modulename);
                // 登录成功，将用户对象添加到 Session
               session.setAttribute("USER_SESSION",userLogin);
               return  "redirect:jsp/index.jsp";
           }
        }
        model.addAttribute("message","  密码或者账户错误");
        return  "/jsp/login";
    }
    //跳转到主页面 /jsp/index.jsp
    @RequestMapping("/homeText")
    public String toHome(){
        return  "/jsp/index";
    }
    //退出页面到登录
    @RequestMapping("/logout")
    public String logout(HttpSession session) {
        // 清除Session
        session.invalidate();
        // 重定向到登录页面的跳转方法
        return "redirect:login";
    }
}