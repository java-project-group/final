package com.hbxy.portal.admin.login.interceptor;

import com.hbxy.portal.admin.org.model.PoUserLogin;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//LoginInterceptor处理不是/loginText的请求，（/loginText，是login.jsp页面提交后返回的页面）,/loginText不拦截但是控制器会处理转到index.jsp页面
//其他地址会判断是否登录，没登录就去login.jsp不运行这个地址
//拦截器
public class LoginInterceptor implements HandlerInterceptor {
    /* 不拦截”/loginText“请求，可以写多个，是个数组 */
    private  static  final String[] IGNORE_URI = { "/loginText"};
    // 该方法将在 Controller 处理前进行调用
    @Override//帮助自己检查是否正确复写了父类的已有方法
    public boolean  preHandle(HttpServletRequest request, HttpServletResponse response,Object o)throws Exception {
        //要于判断是否登录
        boolean flag=false;
        //获取请求的URl
        String url=request.getServletPath();
        //不拦截“/loginText”请求
        // contains（）,该方法是判断字符串中是否有子字符串。如果有则返回true，如果没有则返回false。s每次等于数组里的一个测试是否匹配
        for(String s:IGNORE_URI){
            if(url.contains(s)){
                flag=true;
                break;
            }
        }
        if(flag==false){
            // 获取 Session 并判断是否登录
            PoUserLogin userLogin=(PoUserLogin)request.getSession().getAttribute("USER_SESSION");
            if(userLogin==null){
                request.setAttribute("message","  请先登录");
                // 如果未登录，进行拦截，跳转到登录页面
                request.getRequestDispatcher("/jsp/login.jsp").forward(request,response);
            }else{
                flag=true;
            }
        }
        return  flag;
    }
    public  void postHandle(HttpServletRequest request, HttpServletResponse response, Object o,
                            ModelAndView modelAndView)throws  Exception{
    }
    public void afterCompletion(HttpServletRequest request,HttpServletResponse response,Object o,
                                Exception e)throws Exception{
    }
}
